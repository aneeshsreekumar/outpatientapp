<?php include '../views/header.php';
?>


<div class="container">
          
  <div class="jumbotron" align="center">
    <h3>Register</h3>
   </div>
  <form class="form-horizontal" role="form" action="index.php" method="post">
  <div class="form-group">
    <label class="control-label col-sm-2" for="userlogin">User Login:</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" name="userlogin" id="userlogin" placeholder="Enter User Email" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="password">Password:</label>
    <div class="col-sm-10"> 
      <input type="password" class="form-control" name="password" id="password" required placeholder="Enter password" pattern="[a-zA-Z0-9]{5,12}" title="5-12 alphanumeric characters only allowed">
    </div>
   </div>
   <div class="form-group">
        <label class="control-label col-sm-2" for="password2">Re-enter Password:</label>
    <div class="col-sm-10"> 
      <input type="password" class="form-control" name="password2" id="password2" placeholder="Re-Enter password" required>
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="firstName">First Name:</label>
    <div class="col-sm-10"> 
      <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Enter First Name" required>
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="lastName">Last Name:</label>
    <div class="col-sm-10"> 
      <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Enter Last Name" required>
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="dob">DOB:</label>
    <div class="col-sm-2"> 
      <input type="date" class="form-control" name="dob" id="dob" required>
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="gender">Gender:</label>
    <div class="col-sm-2"> 
      <input type="radio" class="form-control" name="gender" id="gender" value="female" required>Female
      <input type="radio" class="form-control" name="gender" id="gender" value="male" required>Male
    </div>
   </div>

  <div class="form-group">
    <label class="control-label col-sm-2" for="userRole">User Roles:</label>
    <div class="col-sm-10">
      <select name="userRole" class="form-control">
      <?php foreach ($_SESSION['userRoles'] as $eachOption):?>
  		<option value="<?php echo $eachOption['person_code']?>">
  			<?php echo $eachOption['code_description']?>
  		</option>
  		<?php endforeach; ?>
  		</select>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="insurance">Insurance:</label>
    <div class="col-sm-10">
      <select name="insurance" class="form-control">
      <?php foreach ($_SESSION['insurance'] as $eachOption):?>
  		<option value="<?php echo $eachOption['insurance_name']?>">
  			<?php echo $eachOption['insurance_name']?>
  		</option>
  		<?php endforeach; ?>
  		</select>
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" class="btn btn-default" value="Register"/>
    </div>
  </div>
</form>

		
  
</div>
<?php include '../views/footer.php'; ?>