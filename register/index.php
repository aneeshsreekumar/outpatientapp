<?php
require_once('../util/main.php');
require_once('../util/tags.php');
require_once('../model/database.php');
require_once('../model/users_db.php');


/* //echo 'register inside index.php';
var_dump($_POST);
var_dump($_GET); */


if (isset($_GET['action']) and $_GET['action'] == 'register')
{
	//echo 'came inside register action';
	getUserRoles();
	include('register_view.php');
	exit();
}

if(isset($_POST)){
	$userlogin = $_POST['userlogin'];
	$userPwd = $_POST['password'];
	$userRole = $_POST['userRole'];
	$userFname = $_POST['firstName'];
	$userLname = $_POST['lastName'];
	$userDOB = $_POST['dob'];
	$userGender = $_POST['gender'];
	if($userGender == 'male')
		$gender = 'M';
		else if($userGender == 'female')
			$gender = 'F';
	$personid = insertUser($userlogin, $userPwd, $userRole, $userFname, $userLname, $userDOB, $gender);
	if($personid>0){
		//include('../login/login_view.php');
		setSuccess('User Created Successfully. Please login!');
		header("Location: .");
	}else{
		setError('User creation unsuccessful!');
		header("Location: .");
	}
	
	
}
?>