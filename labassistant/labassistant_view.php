<?php include '../views/header.php';?>

<div class="container">
	<div class="jumbotron" align="center">
    <h3>Laboratory</h3>
   </div>

<?php include '../views/usermessages.php';?>

	<table class="table table-bordered" >
		<thead>
			<tr>
				<th colspan=9 align=center>
					Current Test Queue
				</th>
			</tr>
			<tr class='info';>
				<th>Appointment Id</th>
				<th>Doctor Id</th>
				<th>Doctor Name</th>
				<th>Patient Id</th>
				<th>Patient Name</th>
				<th>Test Id</th>
				<th>Test Name</th>
				<th>Appt Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($tests) == 0) : ?>
				<tr class=success><td colspan=9>No tests pending.</td></tr>
			<?php else: ?>
				<?php foreach ($tests as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['appointment_id']?></td>
					<td><?php  echo $row['doctor_id']?></td>
					<td><?php  echo $row['doctor_name']?></td>
					<td><?php  echo $row['patient_id']?></td>
					<td><?php  echo $row['patient_name']?></td>
					<td><?php  echo $row['test_id']?></td>
					<td><?php  echo $row['test_name']?></td>
					<td><?php  echo $row['appt_day']?></td>
					<td>
						<form action="?action=fetchTest" method="get">
							<input type="hidden" name="action" value="fetchTest" />
							<input type="hidden" name="aid" value="<?php echo $row['appointment_id']?>">
							<input type="hidden" name="pid" value="<?php echo $row['patient_id']?>">
							<input type="hidden" name="did" value="<?php echo $row['doctor_id']?>">
							<input type="hidden" name="tid" value="<?php echo $row['test_id']?>">
							<input type="hidden" name="aday" value="<?php echo $row['appt_day']?>">
							<input type=submit value="Fetch Test">
						</form>

					</td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
				<tr>
					<td colspan=9>
				   	<form class="form-horizontal" role="form" action="?action=refreshTests" method="get">
					    <div class="form-group" align="center"> 
				    		<div class="col-sm-1" align="center">
				    			<input type="hidden" name="action" value="refreshTests" />
				      			<input type="submit" class="btn btn-primary" value="Refresh"/>
				    		</div>
				   		</div>
					</form>
					</td>
				</tr>
		</tbody>
    </table>

   
    
    



    <?php if (isset($current_report)) : ?>
    <?php foreach ($current_report as $row) : ?>
    <form class="form-horizontal" role="form" action="?action=updateReport" method="post">
    	<h4>Patient Name: <small><?php echo $row['patient_name'] ?></small></h4>
    	<h4>Doctor Name: <small><?php echo $row['doctor_name'] ?></small></h4>
    	<h4>Test Name: <small><?php echo $row['test_name'] ?></small></h4>
    	<h4>Test Date: <small><?php echo $row['test_date'] ?></small></h4>
    	<h4>Report Id: <small><?php echo $row['report_id'] ?></small></h4>
    	<input type="hidden" name="rid" value="<?php echo $row['report_id'] ?>">
    	<input type="hidden" name="did" value="<?php echo $row['doctor_id'] ?>">
    	<input type="hidden" name="tid" value="<?php echo $row['test_id'] ?>">
    	<input type="hidden" name="action" value="updateReport">
    	<div class="form-group">
	    	<label for=testResult><h4>Test Result: </h4></label>
	    	<select name="testResult" id="testResult" required>
	    		<option value="Negative">Negative</option>
	    		<option value="Positive">Positive</option>
	    	</select>
    	</div>
    	<div class="form-group">
    		<label for=testDetails>Result Details: </label>
    		<textarea class="form-control" id="testDetails" name="testDetails" rows=5 cols=25 required placeholder="Enter Result Details here"></textarea>
    	</div>
    	<div class="form-group" align="center">
    		<input type="submit" value="Update Report">
    	</div>
    </form>
    <?php endforeach; ?>
  	<?php endif; ?>
  	
  	<?php if(isset($message)) : ?>
  	<div class="alert alert-info">
  		<h3><?php echo $message ?></h3>
  	</div>
  	<?php endif; ?>
  	
</div>
<?php include '../views/footer.php'; ?>