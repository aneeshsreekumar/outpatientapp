<?php
require_once('../util/main.php');
require_once('../util/tags.php');
require_once('../model/database.php');
require_once('../model/report_db.php');
require_once('../model/users_db.php');

//$_SESSION['userid'] = 3001;
session_start();
$tests = getNewTests();

if (isset($_GET['action']) and ($_GET['action'] == 'refreshTests') or ($_GET['action'] == 'labassistant'))
{
	$r_id = checkINPReport();
	//echo '->'.$r_id;
	if($r_id != null)
		$current_report = getWorkingReport($r_id);
	include('labassistant_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'viewSearchRequest')
{
	include('lasearchRequests_view.php');
	exit();
}
if (isset($_GET['action']) and $_GET['action'] == 'viewReportsHome')
{
	include('lareports_view.php');
	exit();
}



if (isset($_GET['action']) and $_GET['action'] == 'fetchTest')
{
	$a_id = $_GET['aid'];
	$d_id = $_GET['did'];
	$p_id = $_GET['pid'];
	$t_id = $_GET['tid'];
	$a_day = $_GET['aday'];
	$r_id = fetchNewTest($a_id, $p_id, $d_id, $a_day, $t_id);
	$tests = getNewTests();
	$current_report = getWorkingReport($r_id);
	include('labassistant_view.php');
	exit();
}

if (isset($_POST['action']) and $_POST['action'] == 'updateReport') {
	
	$count = updateReport($_POST['rid'], $_POST['testResult'], $_POST['testDetails']);
	if($count == 1)
		setSuccess("Patient Report details updated successfully");
	else {
		setError("Error updating Patient details");
		$current_report = getWorkingReport($_POST['rid']);
	}
	$tests = getNewTests();
	include('labassistant_view.php');
	exit();
		
}



if (isset($_GET['action']) and $_GET['action'] == 'searchReport')
{
	$patients = searchReport($_GET['lastname']);
	include('lasearchRequests_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'searchReportById')
{
	$patients = searchReportById($_GET['rid']);
	include('lasearchRequests_view.php');
	exit();
}


if (isset($_GET['action']) and $_GET['action'] == 'turnaroundTime')
{
	$reportTAT = getTurnAroundTime();
	include('lareports_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'avgTime')
{
	$reportAvgTAT = getAvgTurnAroundTime();
	include('lareports_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'pendingReports')
{
	$reportsIncomplete = getIncompleteReports();
	include('lareports_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'sameTestReports')
{
	$sameTestMoreReports = getTestsWithMoreThan1Report();
	include('lareports_view.php');
	exit();
}

?>