<?php include '../views/header.php';?>

<div class="container">
	<div class="jumbotron" align="center">
    <h3>Lab Reports</h3>
   </div>

<?php include '../views/usermessages.php';?>

<div class="btn-toolbar">
  		<div class="btn-group">
    
      	<form class="form-horizontal" role="form" action="?action=turnaroundTime" method="get">
	    <div class="form-group" align="center"> 
    		<div class="col-sm-1" align="center">
    			<input type="hidden" name="action" value="turnaroundTime" />
      			<input type="submit" class="btn btn-primary" value="Time for Reports with more than one assistant"/>
    		</div>
   		</div>
		</form>
		<form class="form-horizontal" role="form" action="?action=avgTime" method="get">
	    <div class="form-group" align="center"> 
    		<div class="col-sm-1" align="center">
    			<input type="hidden" name="action" value="avgTime" />
      			<input type="submit" class="btn btn-primary" value="Avg Time Comparison"/>
    		</div>
   		</div>
	    </form>
	    <form class="form-horizontal" role="form" action="?action=sameTestReports" method="get">
	    <div class="form-group" align="center"> 
    		<div class="col-sm-1" align="center">
    			<input type="hidden" name="action" value="sameTestReports" />
      			<input type="submit" class="btn btn-primary" value="More than 1 Test of same Type"/>
    		</div>
   		</div>
		</form>
		
		<form class="form-horizontal" role="form" action="?action=pendingReports" method="get">
	    <div class="form-group" align="center"> 
    		<div class="col-sm-1" align="center">
    			<input type="hidden" name="action" value="pendingReports" />
      			<input type="submit" class="btn btn-primary" value="Pending Reports for more than 1 day"/>
    		</div>
   		</div>
		</form>
   		 </div>
             
    </div>


	
	
	
	
	
	
	
	
  <?php if (isset($reportTAT)) : ?>
    	<table class="table table-bordered" >
		<thead>
			<tr>
				<th colspan=2>
					Time for Reports where more than one assistant worked
				</th>
			</tr>
			<tr class='info';>
				<th>Report Id</th>
				<th>TurnAround Time</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($reportTAT) == 0) : ?>
				<tr class=success><td colspan=2>No Results.</td></tr>
			<?php else: ?>
				<?php foreach ($reportTAT as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['report_id']?></td>
					<td><?php  echo $row['turnaround_time']?></td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>
    
    

<?php if (isset($reportAvgTAT)) : ?>
    <table class="table table-bordered" >
		<thead>
			<tr>
				<th colspan=2>
					Avg Time Comparison for reports with 1 and more than 1 assistant
				</th>
			</tr>
			<tr class='info';>
				<th># of Assistants </th>
				<th>TurnAround Time (in days)</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($reportAvgTAT) == 0) : ?>
				<tr class=success><td colspan=2>No Results.</td></tr>
			<?php else: ?>
				<?php foreach ($reportAvgTAT as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['id']?></td>
					<td><?php  echo $row['avg_time']?></td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>
	
	


	<?php if (isset($sameTestMoreReports)) : ?>
    <table class="table table-bordered" >
		<thead>
			<tr>
				<th colspan=4>
					Tests for which are required in more than one report
				</th>
			</tr>
			<tr class='info';>
				<th>Report Id</th>
				<th>Test Id</th>
				<th>Test Name</th>
				<th>Test Count</th>
				<th>Test Date</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($sameTestMoreReports) == 0) : ?>
				<tr class=success><td colspan=4>No Reports found pending with same tests required.</td></tr>
			<?php else: ?>
				<?php foreach ($sameTestMoreReports as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['report_id']?></td>
					<td><?php  echo $row['test_id']?></td>
					<td><?php  echo $row['test_name']?></td>
					<td><?php  echo $row['test_count']?></td>
					<td><?php  echo $row['test_date']?></td>					
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>

	

	<?php if (isset($reportsIncomplete)) : ?>
    <table class="table table-bordered" >
		<thead>
			<tr>
				<th colspan=4>
					Overdue Reports
				</th>
			</tr>
			<tr class='info';>
				<th>Report Id</th>
				<th>Test Id</th>
				<th>Test Name</th>
				<th>Test Date</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($reportsIncomplete) == 0) : ?>
				<tr class=success><td colspan=4>No Incomplete Reports.</td></tr>
			<?php else: ?>
				<?php foreach ($reportsIncomplete as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['report_id']?></td>
					<td><?php  echo $row['test_id']?></td>
					<td><?php  echo $row['test_name']?></td>
					<td><?php  echo $row['test_date']?></td>					
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>

	

</div>
<?php include '../views/footer.php'; ?>