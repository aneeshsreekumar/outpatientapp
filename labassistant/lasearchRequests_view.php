<?php include '../views/header.php';?>

<div class="container">
	<div class="jumbotron" align="center">
    <h3>Search Lab Requests</h3>
   </div>

<?php include '../views/usermessages.php';?>


<form class="form-horizontal" role="form" action="?action=searchReport" method="get">
	    <div class="form-group" align="center"> 
    		<div class="col-sm-1" align="center">
    			<input type="hidden" name="action" value="searchReport" />
    			<div class="form-group">
	    			<label for="lastname">Patient Last Name: </label>
	    			<input type="text" name=lastname id=lastname placeholder="Enter Patient Last Name here" />
    			</div>
    			<div class="form-group">
      				<input type="submit" class="btn btn-primary" value="Search Report by Last Name"/>
      			</div>
    		</div>
   		</div>
	</form>
 
     <form class="form-horizontal" role="form" action="?action=searchReportById" method="get">
	    <div class="form-group" align="center"> 
    		<div class="col-sm-1" align="center">
    			<input type="hidden" name="action" value="searchReportById" />
    			<div class="form-group">
	    			<label for="rid">Report Id: </label>
	    			<input type="text" name=rid id=rid placeholder="Enter Report Id" />
    			</div>
    			<div class="form-group">
      				<input type="submit" class="btn btn-primary" value="Search Report by Report Id"/>
      			</div>
    		</div>
   		</div>
	</form>
 <?php if (isset($patients)) : ?>
    	<table class="table table-bordered" >
		<thead>
			<tr>
				<th colspan=11>
					Report Information
				</th>
			</tr>
			<tr class='info';>
				<th>Patient Id</th>
				<th>Patient Name</th>
				<th>Report Id</th>
				<th>Test Id</th>
				<th>Test Name</th>
				<th>Test Status</th>
				<th>Patient DOB</th>
				<th>Patient Gender</th>
				<th>Result</th>
				<th>Result Date</th>
				<th>Details</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($patients) == 0) : ?>
				<tr class=success><td colspan=11>No Reports Found.</td></tr>
			<?php else: ?>
				<?php foreach ($patients as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['patient_id']?></td>
					<td><?php  echo $row['patient_name']?></td>
					<td><?php  echo $row['report_id']?></td>
					<td><?php  echo $row['test_id']?></td>
					<td><?php  echo $row['test_name']?></td>
					<td><?php  echo $row['status']?></td>
					<td><?php  echo $row['person_DOB']?></td>
					<td><?php  echo $row['person_gender']?></td>
					<td><?php  echo $row['test_result']?></td>
					<td><?php  echo $row['test_result_date']?></td>
					<td><?php  echo $row['test_result_details']?></td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>
    
</div>
<?php include '../views/footer.php'; ?>