<?php include '../views/header.php';?>

<div class="container">
<div class="jumbotron" align="center">
    <h3>New Appointment</h3>
 </div>
<form class="form-horizontal" role="form" id="newappointmentForm" action="?getDoctorTimeTable" method="post">
  
  <fieldset>
  <div class="form-group">
        <label class="control-label col-sm-2" for="newPatientHistID">Patient History ID:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="newPatientHistID" id="newPatientHistID" value="<?php echo $newPatientHistID;?>"/>
    </div>
   </div>
 
   
   </fieldset>
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedPatient">Tests:</label>
    <div class="col-sm-10">
      <select name="selectedTest" id="selectedTestDropID" class="form-control" required>
      <option value=""></option>
      <?php foreach ($labTests as $eachOption):?>
  		<option value="<?php echo $eachOption['test_id']?>">
  			<?php echo $eachOption['test_name']?>
  		</option>
  		<?php endforeach; ?>
  		</select>
  		<input type="hidden" name="selectedTestValue" id="selectedTestID"/>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="symptomsText">Symptoms:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="symptomsText" id="symptomsText">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="diagnosisText">Diagnosis:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="diagnosisText" id="diagnosisText">
    </div>
  </div>
   
   <div class="form-group">
    <label class="control-label col-sm-2" for="prescriptionText">Prescription:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="prescriptionText" id="prescriptionText">
    </div>
  </div>
  
 
  
<!-- <div class="form-group">
<label class="control-label col-sm-2" for="timeOfApt">Appointment Time:</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="timeOfApt" id="timeOfApt" placeholder="Enter Time" disabled="disabled" required>
</div>
</div>
<div class="form-group">
<label class="control-label col-sm-2" for="dayOfApt">Appointment Day:</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="dayOfApt" id="dayOfApt" placeholder="Enter Day" disabled="disabled" required>
</div>
</div>   -->

    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    	<input type="hidden" name="action" value="updatePatientHistory" />
    	<input type="submit" id="updateRecordButton" class="btn btn-primary" value="Update Record"/>
    </div>
  </div>
</form>


  

</div>
<?php include '../views/footer.php'; ?>