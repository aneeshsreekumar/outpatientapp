<?php include '../views/header.php';
?>


<div class="container">
          
  <div class="jumbotron" align="center">
    <h3>Bill</h3>
   </div>

<?php include '../views/usermessages.php';?>


  <form class="form-horizontal" role="form" action="?action=view" method="get">
  <input type="hidden" name="action" value="view" />
  <fieldset>
  <div class="form-group">
        <label class="control-label col-sm-2" for="appointmentID">Appointment ID:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="appointmentID" id="appointmentID" value="<?php echo trim($billdetails['appointment_id']);?>"/>
    </div>
   </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedPatientValue">Patient Name:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control" name="selectedPatientValue" id="selectedPatientValue" value="<?php echo trim($billdetails['patient_name']);?>">
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="selectedPatientID">Patient ID:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control" name="selectedPatientID" id="selectedPatientID" value="<?php echo trim($billdetails['patient_id']);?>">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="totalDue">Total Due:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="totalDue" id="totalDue" value="<?php  echo $billdetails['total_due']?>"/>
    </div>
   </div>
   
   <div class="form-group">
    <label class="control-label col-sm-2" for="dueNow">Due Now:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="dueNow" id="dueNow" value="<?php  echo $billdetails['due_now']?>"/>
    </div>
   </div>
     
   <div class="form-group">
    <label class="control-label col-sm-2" for="doctorFees">Doctor Fees:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="doctorFees" id="doctorFees" value="<?php  echo $billdetails['doctor_fees']?>"/>
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="testCharge">Test Charge:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="testCharge" id="testCharge" value="<?php  echo $billdetails['test_charge']?>"/>
    </div>
   </div>
    <div class="form-group">
    <label class="control-label col-sm-2" for="insCopay">Insurance Copay:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="insCopay" id="insCopay" value="<?php  echo $billdetails['insurance_copay']?>"/>
    </div>
   </div>
   </fieldset>
    
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" class="btn btn-primary" value="Search Appointment"/>
    </div>
  </div>
</form>

		
  
</div>
<?php include '../views/footer.php'; ?>