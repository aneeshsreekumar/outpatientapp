<?php
require_once('util/main.php');
require_once('util/tags.php');
require_once('model/database.php');
require_once('model/users_db.php');
//echo 'inside main index.php ';

resetSuccess();
resetError();
if (!loginUser())
{
	//header('Location: login/login_view.php');
	include 'login/login_view.php';
	exit();
}
if (isset($_SESSION) and isset($_SESSION['loggedIn']) and $_SESSION['loggedIn'] == TRUE)
 {
  	if($_SESSION['userrole'] == 'STFF')
  		header('Location: appointment/?action=view');
  		//include("appointment/appointment_view.php");
  	else if($_SESSION['userrole'] == 'DOCT')
  		header('Location: doctor/?action=view');
  	else if($_SESSION['userrole'] == 'LABA')
  		header('Location: labassistant/?action=labassistant');
  	else 
  		include 'views/home_view.php';
  	
  	exit();
}
		

// Display the home page
	
?>