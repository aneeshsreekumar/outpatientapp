<?php if (array_key_exists ( 'errorMessage', $GLOBALS ) and ! empty ( $GLOBALS ['errorMessage'] )) : ?>
	<div class="alert alert-danger">
		<strong>
			
		<?php echo $GLOBALS ['errorMessage'];?>
		
		</strong>
	</div>
<?php endif;?>
<?php if (array_key_exists ( 'successMessage', $GLOBALS ) and ! empty ( $GLOBALS ['successMessage'] )) : ?>
	<div class="alert alert-success">
		<strong>
			
		<?php echo $GLOBALS ['successMessage'];?>
		
		</strong>
	</div>
<?php endif;?>