<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo $app_path ?>labassistant/?action=labassistant">Out Patient App</a>
			</div>
			<div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Lab Requests</a>
						<ul class="dropdown-menu">
						<li><a href="<?php echo $app_path ?>labassistant/?action=labassistant">Current Requests</a>
						<li><a href="<?php echo $app_path ?>labassistant/?action=viewSearchRequest">Search Requests</a>
						</ul>
					</li>
					<li class="dropdown"><a href="<?php echo $app_path ?>labassistant/?action=viewReportsHome">Reports</a>
						<ul class="dropdown-menu">
						<li><a href="<?php echo $app_path ?>reports/?action=viewBillReport">Patient Bill</a>
						<li><a href="<?php echo $app_path ?>reports/?action=viewDoctorReport">Doctor</a>
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class="glyphicon glyphicon-user"></span> Profile </a></li>
					<li><a href="<?php echo $app_path ?>logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout </a></li>
				</ul>
			</div>
		</div>
</nav>