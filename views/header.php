<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Out Patient App</title>

    <!-- Bootstrap -->
    <link href="<?php echo $app_path ?>client/bootstrap-3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $app_path ?>client/css/app.css" rel="stylesheet">
  </head>
  <body>
  <?php 
  if (isset($_SESSION) and isset($_SESSION['loggedIn']) and $_SESSION['loggedIn'] == TRUE)
  {
  	if($_SESSION['userrole'] == 'STFF')
  		include 'menubar_view_staff.php';
  	else if($_SESSION['userrole'] == 'DOCT')
  		include 'menubar_view_doc.php';
  	else if($_SESSION['userrole'] == 'LABA')
  		include 'menubar_view_la.php';
  	else 
  		include 'menubar_view.php';
  }
  else
  {
  	// don't include menu
  } 
  
  ?>