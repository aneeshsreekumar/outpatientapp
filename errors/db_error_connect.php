<?php include 'views/header.php'; ?>
<div class="container">
    <h1>Database Error</h1>
    <p>An error occurred connecting to the database.</p>
    <p>The database must be installed as described in the appendix.</p>
    <p>Error message: <?php echo $error_message; ?></p>
    <p>&nbsp;</p>
</div><!-- end content -->
<?php include 'views/footer.php'; ?>