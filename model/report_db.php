<?php


function getNewTests() {
	global $db;
	$query = 'select ph.appointment_id, ph.doctor_id, concat(d.person_first_name, " ", d.person_last_name) as doctor_name, ph.patient_id, concat(p.person_first_name, " ", p.person_last_name) as patient_name, ph.test_id, lt.test_name, a.appt_day
from patient_history ph join person p
on ph.patient_id = p.person_id

join person d
on ph.doctor_id = d.person_id

join appointment a
on ph.appointment_id = a.appointment_id

join laboratory_test lt
on ph.test_id = lt.test_id

where (ph.test_id is not null) and (ph.report_id is null)';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

//check if there exists any report being worked upon
function checkINPReport() {
	global $db;
	$query = 'select lr.report_id 
from lab_assignment la

join laboratory_report lr
on lr.report_id = la.report_id

where lr.status = "INP"
and la.lab_assistant_id = :userid';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':userid', $_SESSION['userid']);
		$statement->execute();
		$result = $statement->fetchColumn();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

//GEt current working report to be displayed in the form
function getWorkingReport($report_id) {
	global $db;
	$query = 'select lr.report_id, lr.patient_id, lr.doctor_id, lr.test_date, lr.test_id, lt.test_name,
concat(d.person_first_name, " ", d.person_last_name) as doctor_name, concat(p.person_first_name, " ", p.person_last_name) as patient_name
from laboratory_report lr
join person p
on lr.patient_id = p.person_id

join person d
on lr.doctor_id = d.person_id

join laboratory_test lt
on lr.test_id = lt.test_id

where status = "INP"
and report_id = :report_id';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':report_id', $report_id);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}


// called when a lab assistant clicks 'fetch the test to work on' for a given test in queue
function fetchNewTest($appointment_id, $patient_id, $doctor_id, $test_date, $test_id) {
	global $db;
	$query = 'insert into laboratory_report(patient_id, doctor_id, test_date, test_id, status)
values(:patient_id, :doctor_id, :test_date, :test_id, "INP")';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':patient_id', $patient_id);
		$statement->bindValue(':doctor_id', $doctor_id);
		$statement->bindValue(':test_date', $test_date);
		$statement->bindValue(':test_id', $test_id);
		$value = $statement->execute();
		echo 'result of execution:

				';
		var_dump($value);
		$statement->closeCursor();
		$report_id = $db->lastInsertId();
// 		return $report_id;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
	
	assignLabAssistant($report_id, $_SESSION['userid']);
	
	$query = 'update patient_history
set report_id = :report_id
where appointment_id = :appointment_id';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':appointment_id', $appointment_id);
		$statement->bindValue(':report_id', $report_id);
		$row_count = $statement->execute();
		$statement->closeCursor();
		
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
	return $report_id;
}

// assign an assistant to a report
function assignLabAssistant($report_id, $assistant_id) {
	global $db;
	$query = 'insert into lab_assignment(lab_assistant_id, report_id) values(:lab_assistant_id, :report_id)';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':lab_assistant_id', $assistant_id);
		$statement->bindValue(':report_id', $report_id);
		$value = $statement->execute();
		echo 'result of execution:

				';
		var_dump($value);
		$statement->closeCursor();
// 		$report_id = $db->lastInsertId();
// 		return $report_id;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function searchReport($lastname) {
	
	global $db;
	$query = 'select lr.patient_id, concat(p.person_first_name, " ", p.person_last_name) as patient_name, lr.report_id,  lt.test_id, lt.test_name, lr.status,
	p.person_DOB, p.person_gender, lr.test_result, lr.test_result_date, lr.test_result_details
	from person p
	
	join patient_history ph
	on p.person_id = ph.patient_id
	
	join laboratory_test lt
	on ph.test_id = lt.test_id
	
	join laboratory_report lr
	on ph.report_id = lr.report_id
	
	where lower(p.person_last_name) like concat("%", :lastname,"%")
	order by lr.test_date desc';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':lastname', $lastname);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}


function searchReportById($id) {

	global $db;
	$query = 'select lr.patient_id, concat(p.person_first_name, " ", p.person_last_name) as patient_name, lr.report_id,  lt.test_id, lt.test_name, lr.status,
	p.person_DOB, p.person_gender, lr.test_result, lr.test_result_date, lr.test_result_details
	from person p

	join patient_history ph
	on p.person_id = ph.patient_id

	join laboratory_test lt
	on ph.test_id = lt.test_id

	join laboratory_report lr
	on ph.report_id = lr.report_id

	where ph.report_id = :report_id
	order by lr.test_date desc';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':report_id', $id);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function updateReport($report_id, $result, $details) {
	global $db;
	$query = 'update laboratory_report
set test_result_date = now(),
test_result = :result,
test_result_details = :details,
status = "COM"
where report_id = :report_id';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':result', $result);
		$statement->bindValue(':report_id', $report_id);
		$statement->bindValue(':details', $details);
		$row_count = $statement->execute();
		$statement->closeCursor();
		return $row_count;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
	
}

function getTestsWithMoreThan1Report() {
	global $db;
	$query = 'select l.report_id, tab.test_id, tab.test_name, tab.test_count, l.test_date from laboratory_report l
join 
(select lr.test_id, lt.test_name, count(lr.test_id) as test_count
from laboratory_report lr join laboratory_test lt
on lr.test_id = lt.test_id
where lower(lr.status) = "inp"
group by lr.test_id, lt.test_name
having count(*)>1) tab
on l.test_id = tab.test_id

where lower(l.status) = "inp"';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function getIncompleteReports() {
	global $db;
	$query = 'select lr.report_id, lr.test_id, lt.test_name, lr.test_date
from laboratory_report lr join laboratory_test lt
on lr.test_id = lt.test_id
where lower(lr.status) = "inp"
and datediff(date(now()), test_date) > 1';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getTurnAroundTime() {
	global $db;
	$query = 'select lr.report_id, datediff(lr.test_result_date, lr.test_date) turnaround_time
from laboratory_report lr

join lab_assignment la
on lr.report_id = la.report_id

group by report_id
having count(la.lab_assistant_id) > 1';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getAvgTurnAroundTime() {
	global $db;
	$query = 'select id, avg(turnaround_time) avg_time from 
(
select lr.report_id, "more than one assistant" as id, datediff(lr.test_result_date, lr.test_date) turnaround_time
from laboratory_report lr

join lab_assignment la
on lr.report_id = la.report_id

group by report_id
having count(la.lab_assistant_id) > 1

union

select lr.report_id, "one assistant" as id, datediff(lr.test_result_date, lr.test_date) turnaround_time
from laboratory_report lr

join lab_assignment la
on lr.report_id = la.report_id

group by report_id
having count(la.lab_assistant_id) = 1
) t
group by id';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}


