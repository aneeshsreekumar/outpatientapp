<?php
$dsn = 'mysql:host=localhost;dbname=outpatientdbnew';
$username = 'root';
$password = 'root@1314';
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
//echo 'came inside database';
try {
    $db = new PDO($dsn, $username, $password, $options);
    //echo 'after connecting to database';
} catch (PDOException $e) {
    $error_message = $e->getMessage();
    include '../errors/db_error_connect.php';
    exit;
}

function display_db_error($error_message) {
    global $app_path;
    include '../errors/db_error.php';
    exit;
}
?>