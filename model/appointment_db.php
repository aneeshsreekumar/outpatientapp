<?php
function getTimeTable($selectedDoctor) {
	global $db;
	$query = 'SELECT concat(person_first_name," ",person_last_name) doctor_name,t.*
FROM
timetable t

join person p
on t.doctor_id = p.person_id

join doctor d
on p.person_id = d.doctor_id
and d.doctor_id = :selectedDoctor
order by t.doctor_id, t.timeslot';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':selectedDoctor', $selectedDoctor);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getTimeTableFromTime($selectedDoctor,$fromtime) {
	global $db;
	$query = 'SELECT concat(person_first_name," ",person_last_name) doctor_name,t.*
FROM
timetable t

join person p
on t.doctor_id = p.person_id

join doctor d
on p.person_id = d.doctor_id
and d.doctor_id = :selectedDoctor
and t.timeslot > :fromtime
order by t.doctor_id, t.timeslot';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':selectedDoctor', $selectedDoctor);
		$statement->bindValue(':fromtime', $fromtime);	
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getAppointments() {
	global $db;
	$query = 'select a.appointment_id, concat(patient.person_first_name," ", patient.person_last_name) as patient_name,
concat(doctor.person_first_name," ", doctor.person_last_name) as doctor_name, a.appt_time, a.appt_day, a.status
from appointment a, person as patient, person as doctor
where a.patient_id = patient.person_id
and a.doctor_id = doctor.person_id
order by appt_day desc,appt_time desc';
	try {
		$statement = $db->prepare($query);

		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getAppointmentsForDoctor($doctorID) {
	global $db;
	$query = 'select a.appointment_id, concat(patient.person_first_name," ", patient.person_last_name) as patient_name,
concat(doctor.person_first_name," ", doctor.person_last_name) as doctor_name, a.appt_time, a.appt_day, a.status
from appointment a, person as patient, person as doctor
where a.patient_id = patient.person_id
and a.doctor_id = doctor.person_id
and a.doctor_id=:doctorID			
order by appt_day desc,appt_time desc';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':doctorID', $doctorID);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getAppointmentById($appointmentID) {
	global $db;
	$query = 'select a.appointment_id, concat(patient.person_first_name," ", patient.person_last_name) as patient_name, patient.person_id as patient_id,
concat(doctor.person_first_name," ", doctor.person_last_name) as doctor_name, doctor.person_id as doctor_id, a.appt_time, a.appt_day, a.status, ai.ailment_description, ai.ailment_id
from appointment a, person as patient, person as doctor, ailment ai
where a.patient_id = patient.person_id
and a.doctor_id = doctor.person_id
and a.ailment_id= ai.ailment_id
and a.appointment_id=:appointmentID';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':appointmentID', $appointmentID);

		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result[0];
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getAppointmentsForPatient($firstname) {
	global $db;
	$query = 'select a.appointment_id, concat(patient.person_first_name," ", patient.person_last_name) as patient_name,
concat(doctor.person_first_name," ", doctor.person_last_name) as doctor_name, a.appt_time, a.appt_day, a.status
from appointment a, person as patient, person as doctor
where a.patient_id = patient.person_id
and a.doctor_id = doctor.person_id
and UPPER(patient.person_first_name) = :firstname
order by appt_time';
	$firstname = strtoupper($firstname);
	//echo 'firstname:'.$firstname;
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':firstname', $firstname);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function newAppointment($patient_id, $doctor_id, $ailment_id, $appt_time, $appt_day) {
	global $db;
	$query = 'insert into appointment(patient_id, staff_id, doctor_id, appt_time, appt_day, status,ailment_id)
values(:patient_id, :staff_id, :doctor_id, :appt_time, :appt_day, "SCH", :ailment_id)';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':patient_id', $patient_id);
		$statement->bindValue(':doctor_id', $doctor_id);
		$statement->bindValue(':staff_id', $_SESSION['userid']);
		$statement->bindValue(':appt_time', $appt_time);
		$statement->bindValue(':appt_day', $appt_day);
		$statement->bindValue(':ailment_id', $ailment_id);
		$value = $statement->execute();
		//var_dump($value);
		$statement->closeCursor();
		$appointment_id = $db->lastInsertId();
		return $appointment_id;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function cancelAppointment($appointment_id) {
	global $db;
	$query = 'update appointment
set status = "CAN"
where appointment_id = :appointment_id';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':appointment_id', $appointment_id);
		$row_count = $statement->execute();
		$statement->closeCursor();
		return $row_count;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function rescheduleAppointment($appointment_id, $appt_day, $appt_time) {
	global $db;
	$query = 'update appointment
set appt_day = :appt_day,
 appt_time = :appt_time
where appointment_id = :appointment_id;';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':appointment_id', $appointment_id);
		$statement->bindValue(':appt_day', $appt_day);
		$statement->bindValue(':appt_time', $appt_time);
		$row_count = $statement->execute();
		$statement->closeCursor();
		return $row_count;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function getDoctorAvailabilityList() {
	global $db;
	$query = 'select concat(p.person_first_name, " ", p.person_last_name) as doctor_name, 
		a.availability_group, availability_start_time, availability_end_time
	from doctor d, availability a, person p
	where d.doctor_id = p.person_id
	and d.availability_id = a.availability_id';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function getAilments() {
	global $db;
	$query = 'SELECT * FROM ailment order by ailment_description';
	try {
		$statement = $db->prepare($query);

		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();

		return $result;

	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function getBillforCustomer($appointment_id) {
	global $db;
	$query = 'select a.appointment_id, concat(pe.person_first_name, " ", pe.person_last_name) as patient_name, a.patient_id,
	sum(fees + test_charge) as total_due, 
    if(insurance_copay is null, fees + test_charge, (fees + test_charge) - insurance_copay) as due_now,
    d.fees as doctor_fees, 
    lt.test_charge, 
    ifnull(i.insurance_copay, 0) as insurance_copay
from appointment a join doctor d
on a.doctor_id = d.doctor_id

join patient_history ph
on a.appointment_id = ph.appointment_id

join patient p
on a.patient_id = p.patient_id

join person pe
on pe.person_id = p.patient_id

left outer join laboratory_test lt
on ph.test_id = lt.test_id

left outer join insurance i
on p.insurance_id = i.insurance_id

where a.appointment_id = :appointment_id';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':appointment_id', $appointment_id);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result[0];
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

// get patient's report status
function getPatientReportStatus() {
	global $db;
	$query = 'select concat(p.person_first_name, " ", p.person_last_name), p.person_DOB, p.person_gender,
	lt.test_name, ph.report_id, lr.status
from person p

join patient_history ph
on p.person_id = ph.patient_id

join laboratory_test lt
on ph.test_id = lt.test_id

join laboratory_report lr
on ph.report_id = lr.report_id

where lower(p.person_last_name) like concat("%", :lastname,"%")
		order by lr.test_date desc';
	try {
		$statement = $db->prepare($query);

		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}
//-- which doctor ordered maximum number of atype of test and the department to which he/she belongs
function reportTestStats() {
	global $db;
	$query = 'select tab.doctor_id, concat(p.person_first_name," ", p.person_last_name) as doctor_name, department_name, test_cnt, test_name
from
(select doctor_id, test_id, count(*) test_cnt
from patient_history ph
group by doctor_id, test_id
having (test_id, test_cnt) in 
(select test_id, max(test_cnt) max_cnt from 
	(select doctor_id, test_id, count(*) as test_cnt
	from patient_history ph
	group by doctor_id, test_id) t
    group by test_id
)) tab
join person p
on tab.doctor_id = p.person_id

join laboratory_test lt
on tab.test_id = lt.test_id

join doctor d
on tab.doctor_id = d.doctor_id

join department dep
on d.department_id = dep.department_id';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}
?>