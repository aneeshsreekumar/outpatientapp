<?php
function getUser($userLogin) {
    global $db;
    //echo 'came inside getUser userLogin:';
    //var_dump($userLogin);
    //echo 'came inside getUser userPWD:';
    
    $query = 'SELECT * FROM person
              WHERE person_email = :userlogin';
    try {
        $statement = $db->prepare($query);
        $statement->bindValue(':userlogin', $userLogin);
        $statement->execute();
        $result = $statement->fetchAll();
        $statement->closeCursor();
        
        if ($result > 0)
        {
        	//echo 'came inside if';
        	//var_dump($result);
        	return $result[0];
        }
        else
        {
        	return null;
        }
        
        return null;
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
        display_db_error($error_message);
    }
}

function insertUser($userlogin, $userPwd, $userRole, $userFname, $userLname, $userDOB, $gender) {
	global $db;
		
	//echo $userlogin.$userPwd.$userRole;
	$hashPwd=password_hash($userPwd,PASSWORD_DEFAULT);
	
	$query = 'INSERT INTO person(person_id,person_first_name, person_last_name, person_DOB, person_type, person_email, person_gender, password) 
			VALUES(0,:userFname, :userLname,:userDOB, :userRole, :userlogin, :gender, :hashPwd)'; 
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':userFname', $userFname);
		$statement->bindValue(':userLname', $userLname);
		$statement->bindValue(':userDOB', $userDOB);
		$statement->bindValue(':userRole', $userRole);
		$statement->bindValue(':userlogin', $userlogin);
		$statement->bindValue(':gender', $gender);
		$statement->bindValue(':hashPwd', $hashPwd);
		$value = $statement->execute();
		//echo 'result of execution:
				
		//		';
		//var_dump($value);
		$statement->closeCursor();

		$person_id = $db->lastInsertId();
		return $person_id;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function getUserRoles() {
	global $db;
	$query = 'SELECT * FROM person_type order by code_description';
	try {
		$statement = $db->prepare($query);
		
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();

		if ($result > 0)
		{
			$_SESSION['userRoles'] = $result;
			
		}
		
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function getInsuranceInfo() {
	global $db;
	$query = 'SELECT insurance_name FROM insurance';
	try {
		$statement = $db->prepare($query);

		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();

		if ($result > 0)
		{
			$_SESSION['insurance'] = $result;
				
		}

	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function userIsLoggedIn()
{
  if (isset($_GET['action']) and $_GET['action'] == 'logout')
  {
    session_start();
    unset($_SESSION['loggedIn']);
    unset($_SESSION['userlogin']);
    unset($_SESSION['password']);
    unset($_SESSION['userrole']);
    header('Location: ' . $_POST['goto']);
    exit();
  }
  
  /* if (isset($_SESSION['loggedIn']))
  {
    return isValidUser($_SESSION['userlogin'], $_SESSION['password']);
  }  */
}




function loginUser(){
	//echo 'came inside userISlogged In';
	if (isset($_POST['action']) and $_POST['action'] == 'login')
	{
		//echo 'came inside post login USser In';
		//var_dump($_POST);
		if (!isset($_POST['userlogin']) or $_POST['userlogin'] == '' or
				!isset($_POST['password']) or $_POST['password'] == '')
		{
			$GLOBALS['errorMessage'] = 'Please fill in both fields';
			return FALSE;
		}
	
		$userPwd = $_POST['password'];
		
		$user = getUser($_POST['userlogin']);
		//echo 'userPwd:'.$userPwd;
		//echo 'hashed pwd'.$user['password'];
		if ($user != null and password_verify($userPwd,$user['password']))
		{
			//echo 'password verified';
			session_start();
			$_SESSION['loggedIn'] = TRUE;
			$_SESSION['userlogin'] = $_POST['userlogin'];
			$_SESSION['password'] = $user['password'];
			$_SESSION['userrole'] = $user['person_type'];
			$_SESSION['userid'] = $user['person_id'];
			return TRUE;
		}
		else
		{
			//echo 'password failed';
			session_start();
			unset($_SESSION['loggedIn']);
			unset($_SESSION['userlogin']);
			unset($_SESSION['password']);
			unset($_SESSION['userrole']);
			unset($_SESSION['userid']);
			$GLOBALS['errorMessage'] =
			'The specified user login or password was incorrect.';
			return FALSE;
		}
	}else{
		return FALSE;
	}
}

function isValidUser($userlogin, $password)
{
  global $db;

  try
  {
    $sql = 'SELECT COUNT(*) FROM user
        WHERE user_login = :userlogin AND password = :password';
    $s = $db->prepare($sql);
    $s->bindValue(':userlogin', $userlogin);
    $s->bindValue(':password', $password);
    $s->execute();
  }
  catch (PDOException $e)
  {
    $error = 'Error searching for author.';
    include 'error.html.php';
    exit();
  }

  $row = $s->fetch();

  if ($row[0] > 0)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

function userHasRole($role)
{
  include 'db.inc.php';

  try
  {
    $sql = "SELECT COUNT(*) FROM author
        INNER JOIN authorrole ON author.id = authorid
        INNER JOIN role ON roleid = role.id
        WHERE email = :email AND role.id = :roleId";
    $s = $pdo->prepare($sql);
    $s->bindValue(':email', $_SESSION['email']);
    $s->bindValue(':roleId', $role);
    $s->execute();
  }
  catch (PDOException $e)
  {
    $error = 'Error searching for author roles.';
    include 'error.html.php';
    exit();
  }

  $row = $s->fetch();

  if ($row[0] > 0)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

function getPerson($type) {
	global $db;
	$query = 'select concat(p.person_first_name, " ", p.person_last_name) as personname,p.person_id';
	if($type == 'DOCT'){
		$query = $query.' from doctor d, person p
	where d.doctor_id = p.person_id
	and p.person_type = :type order by personname';
	}else if ($type == 'PTNT'){
		$query = $query.' from patient d, person p
	where d.patient_id = p.person_id
	and p.person_type = :type order by personname';
	}else if ($type == 'STFF'){
		$query = $query.' from patient d, person p
	where d.patient_id = p.person_id
	and p.person_type = :type order by personname';
	}
	//echo 'query:'.$query;
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':type', $type);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}

}

function deleteUser($userID){
	global $db;
	$query = 'DELETE FROM person WHERE person_id = :personID;';
	
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':personID', $userID);
		$result = $statement->execute();
		//var_dump($result);
		$statement->closeCursor();
		
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
	
}

?>