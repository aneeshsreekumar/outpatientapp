<?php
// to be called when the doctor clicks the patient history button on patient screen that appears after 'Next Patient' is pressed
function getPatientHistory() {
	global $db;
	$query = 'select a.appt_day, ph.symptoms, ph.diagnosis, ph.prescription, lt.test_name, lr.test_result
from patient_history ph

join appointment a
on ph.appointment_id = a.appointment_id

left outer join laboratory_test lt
on ph.test_id = lt.test_id

left outer join laboratory_report lr
on ph.report_id = lr.report_id

order by appt_day';
	try {
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function insertPatientHistory($patient_id,$appointment_id,$doctor_id,$ailment_id,$symptoms,$diagnosis,$prescription,$test_id){
	global $db;
	$query = 'insert into patient_history(patient_id, appointment_id, doctor_id, ailment_id,symptoms,diagnosis,prescription,test_id,report_id)
	values(:patient_id, :appointment_id, :doctor_id, :ailment_id,:symptoms,:diagnosis,:prescription,:test_id,:report_id);';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':patient_id', $patient_id);
		$statement->bindValue(':appointment_id', $appointment_id);
		$statement->bindValue(':doctor_id', $doctor_id);
		$statement->bindValue(':ailment_id', $ailment_id);
		$statement->bindValue(':symptoms', $symptoms);
		$statement->bindValue(':diagnosis', $diagnosis);
		$statement->bindValue(':prescription', $prescription);
		$statement->bindValue(':test_id', $test_id);
		$statement->bindValue(':report_id', null);
		$value = $statement->execute();
		//var_dump($value);
		$statement->closeCursor();
		$patient_historyid = $db->lastInsertId();
		return $patient_historyid;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
	
	
}
//to be called when the doctor hits 'Next Patient' on the patient form
function getNextPatient($doctor_id) {
	global $db;
	$query = 'select a.appointment_id, a.patient_id, concat(p.person_first_name, " ", p.person_last_name) as patient_name, a.appt_time, al.ailment_description
from appointment a

join ailment al
on a.ailment_id = al.ailment_id

join person p
on a.patient_id = p.person_id

join patient_history ph
on a.patient_id = ph.patient_id

where a.doctor_id = :doctor_id
and a.status = "SCH"';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':doctor_id', $doctor_id);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}


// to be called when the doctor hits 'Update' on the patient form
function updatePatientHistory($patient_id,$appointment_id, $symptoms, $diagnosis, $prescription, $test_id ) {
	global $db;
	$query = 'update patient_history
set symptoms = :symptoms, diagnosis = :diagnosis, prescription = :prescription, test_id = :test_id
where appointment_id = :appointment_id and patient_id=:patient_id';
	try {
		$statement = $db->prepare($query);
		$statement->bindValue(':patient_id', $patient_id);
		$statement->bindValue(':appointment_id', $appointment_id);
		$statement->bindValue(':symptoms', $symptoms);
		$statement->bindValue(':diagnosis', $diagnosis);
		$statement->bindValue(':prescription', $prescription);
		$statement->bindValue(':test_id', $test_id);
		$row_count = $statement->execute();
		$statement->closeCursor();
		return $row_count;
	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}

function getTests() {
	global $db;
	$query = 'SELECT * FROM laboratory_test order by test_name';
	try {
		$statement = $db->prepare($query);

		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();

		return $result;

	} catch (PDOException $e) {
		$error_message = $e->getMessage();
		display_db_error($error_message);
	}
}