<?php include '../views/header.php';?>

<div class="container">
<div class="jumbotron" align="center">
    <h3>Book Time Slot</h3>
 </div>
<form class="form-horizontal" role="form" id="doctortimetableform" action="?createAppointment" method="post">
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedPatient">Patient:</label>
    <div class="col-sm-10">
	<input type="text" class="form-control" name="selectedPatient" id="selectedPatientDocTime" value=<?php echo $selectedPatientValue?> disabled="disabled">
	</div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedAilment">Ailment:</label>
    <div class="col-sm-10">
	<input type="text" class="form-control" name="selectedAilment" id="selectedAilmentDocTime" value=<?php echo $selectedAilmentValue?> disabled="disabled">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedDoctor">Doctor:</label>
    <div class="col-sm-10">
	<input type="text" class="form-control" name="selectedDoctor" id="selectedDoctorDocTime" value=<?php echo $selectedDoctorValue?> disabled="disabled">
    </div>
  </div>
  
  <input type="hidden" name="selectedTimeSlot" id="selectedTimeSlot" />
  <input type="hidden" name="selectedDay" id="selectedDay"/>
  
  <?php if (isset($timetable)) : ?>
	<table class="table table-bordered table-striped" >
		<thead title="Doctor TimeTable">
			<tr>
				<th>Time Slot</th>
				<th>Monday</th>
				<th>Tuesday</th>
				<th>Wednesday</th>
				<th>Thursday</th>
				<th>Friday</th>
				<th>Saturday</th>
				<th>Sunday</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($timetable) == 0) : ?>
				<p>Error Fetching Time Table. Please contact System Administrator</p>
			<?php else: ?>
				<?php foreach ($timetable as $row) : ?>
				<tr>
					<td><?php  echo $row['timeslot']?></td>
					<td><?php  if ($row['monday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(2,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
					<td><?php  if ($row['tuesday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(3,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
					<td><?php  if ($row['wednesday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(4,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
					<td><?php  if ($row['thursday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(5,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
					<td><?php  if ($row['friday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(6,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
					<td><?php  if ($row['saturday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(7,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
					<td><?php  if ($row['sunday'] == 0): echo 'NA'; 
					else: ?><input type="button" class="btn btn-success" value="Book" onclick="bookAppointment(1,'<?php  echo $row['timeslot']?>')"/>
					<?php endif;?></td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>
    
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      
    </div>
  </div>
</form>

    

</div>
<?php include '../views/footer.php'; ?>