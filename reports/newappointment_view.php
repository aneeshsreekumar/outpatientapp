<?php include '../views/header.php';?>

<div class="container">
<div class="jumbotron" align="center">
    <h3>New Appointment</h3>
 </div>
<form class="form-horizontal" role="form" id="newappointmentForm" action="?getDoctorTimeTable" method="post">
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedPatient">Patient:</label>
    <div class="col-sm-10">
      <select name="selectedPatient" id="selectedPatientDropID" class="form-control" required>
      <option value=""></option>
      <?php foreach ($patients as $eachOption):?>
  		<option value="<?php echo $eachOption['person_id']?>">
  			<?php echo $eachOption['personname']?>
  		</option>
  		<?php endforeach; ?>
  		</select>
  		<input type="hidden" name="selectedPatientValue" id="selectedPatientID"/>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedAilment">Ailment:</label>
    <div class="col-sm-10">
      <select name="selectedAilment" id="selectedAilmentDropID" class="form-control" required>
      <option value=""></option>
      <?php foreach ($ailments as $eachOption):?>
  		<option value="<?php echo $eachOption['ailment_id']?>">
  			<?php echo $eachOption['ailment_description']?>
  		</option>
  		<?php endforeach; ?>
  		</select>
  		<input type="hidden" name="selectedAilmentValue" id="selectedAilmentID"/>
    </div>
  </div>
  
<!-- <div class="form-group">
<label class="control-label col-sm-2" for="timeOfApt">Appointment Time:</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="timeOfApt" id="timeOfApt" placeholder="Enter Time" disabled="disabled" required>
</div>
</div>
<div class="form-group">
<label class="control-label col-sm-2" for="dayOfApt">Appointment Day:</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="dayOfApt" id="dayOfApt" placeholder="Enter Day" disabled="disabled" required>
</div>
</div>   -->

 <div class="form-group">
	<label class="control-label col-sm-2" for="selectedDoctor">Doctor:</label>
	<div class="col-sm-10">
	<select name="selectedDoctor" id="selectedDoctorDropID" class="form-control">
	<?php foreach ($doctors as $eachOption):?>
  		<option value="<?php echo $eachOption['person_id']?>">
  			<?php echo $eachOption['personname']?>
  		</option>
  		<?php endforeach; ?>
  		</select>
  		<input type="hidden" name="selectedDoctorValue" id="selectedDoctorID"/>
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    	<input type="hidden" name="action" value="getDoctorTimeTable" />
    	<input type="button" id="viewAvailabilityButton" class="btn btn-default" value="View Availability"/>
    </div>
  </div>
</form>

<form class="form-horizontal" role="form" action="?getDoctorTimeTable" method="get">
		<input type="hidden" name="action" value="getDoctorTimeTable" />
										
	
    
  </form>
  

</div>
<?php include '../views/footer.php'; ?>