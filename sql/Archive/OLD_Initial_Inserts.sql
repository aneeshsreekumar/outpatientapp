INSERT INTO `ailment` (`ailment_id`,`ailment_description`) VALUES (1,'GERD');
INSERT INTO `ailment` (`ailment_id`,`ailment_description`) VALUES (2,'Common Cold');
INSERT INTO `ailment` (`ailment_id`,`ailment_description`) VALUES (3,'Ear Infection');
INSERT INTO `ailment` (`ailment_id`,`ailment_description`) VALUES (4,'Allergy');
INSERT INTO `ailment` (`ailment_id`,`ailment_description`) VALUES (5,'Asthma');
INSERT INTO `ailment` (`ailment_id`,`ailment_description`) VALUES (6,'Flu');

INSERT INTO `person_type` (`person_code`, `code_description`, `code_created`) VALUES ('DOCT', 'Doctor', now());
INSERT INTO `person_type` (`person_code`, `code_description`, `code_created`) VALUES ('PTNT', 'Patient', now());
INSERT INTO `person_type` (`person_code`, `code_description`, `code_created`) VALUES ('LABA', 'Lab Assistant', now());
INSERT INTO `person_type` (`person_code`, `code_description`, `code_created`) VALUES ('PHRM', 'Pharmacist', now());
INSERT INTO `person_type` (`person_code`, `code_description`, `code_created`) VALUES ('STFF', 'Staff', now());

INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (1,'Doctor1','John','1970-01-13','DOCT','1','999-999-9999','dr@g.com','M','$2y$10$foBbCRxzKEwmgU3wji.4YecWGNRXzu6f24Er4.2o5uLBM9iclsnhG');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (2,'Patient1','Bob','1980-01-13','PTNT','2','999-999-9999','p@g.com','M','$2y$10$a3jLEc63JQNXIFqmlsOR7uvXHQqAiJfBpypSzX3pgvrAkUoPwrWPS');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (3,'Staff1','Mary','1980-01-13','STFF','3','999-999-9999','s@g.com','F','$2y$10$rxGW8/RwTeiEeHnslHzrPOSzjBADW/w1.x7Z1QLQM1wYhxwYes65y');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (4,'LabA1','Diane','1980-01-15','LABA','4','999-999-9999','la@g.com','F','$2y$10$8t/YKIakmMTPmowhpWQA1eJVWzLQoS7eX2inXO/o7YeApho/ugHC6');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (5,'LabA2','Chris','1980-01-16','LABA','5','888-888-8888','la2@g.com','M','$2y$10$pMgtO4lyPVqkBGKdqMt32O0J/dXKDXoa6TgpWUBe2B/JuKFiFT9S2');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (6,'LabA3','Paul','1980-01-16','LABA','6','888-888-8888','la3@g.com','M','$2y$10$J4fq.AcPzailuYcCi51ARer4ikUsAgqEXDKuIQ36ZCpGLqnci8DHu');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (7,'Patient2','Britney','1980-01-16','PTNT','7','888-888-8888','p2@g.com','F','$2y$10$V3Lao9U3OvCz1oq4fZe16.mFhaTfkZc39pxU/lR7x22Z34wmkp0Ue');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (8,'Patient3','Smith','1980-01-16','PTNT','8','888-888-8888','p3@g.com','M','$2y$10$RdOHELnbSghvEPdq/GZi/.nkkWi1I7PhpUiLQQQQsqP0dTDFuBhmi');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (9,'Patient4','Marie','1980-01-16','PTNT','9','888-888-8888','p4@g.com','F','$2y$10$x9d79eOmProFT6e7iCBt3eto/LCPh2ZT3v8/ACyDFIuH4qM0onc3S');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (10,'Patient5','Sno','1980-01-16','PTNT','10','888-888-8888','p5@g.com','M','$2y$10$In14l6ol/gDpAdv0q2IkreIuk.Wc4.DFSd.6bPO1nFsoZDGFeD0V6');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (11,'Doctor2','Lannister','1980-01-16','DOCT','11','888-888-8888','dr1@g.com','F','$2y$10$AQwiNG9VsCP/G0.iM/JykeTDHYT4V/jXrinmFUyNb/pu5dtBk009C');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (12,'Doctor3','Stark','1980-01-16','DOCT','12','888-888-8888','dr2@g.com','M','$2y$10$y4U44sOWyW177n.F2nKTZOK005UIQHFVizYFJG3sU6bTz0jgMNjba');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (13,'Doctor4','Tyrell','1980-01-16','DOCT','13','888-888-8888','dr3@g.com','F','$2y$10$vwaRqnUI.ykancJ/r.hjZOvAJMsW/3VRsBnukx4SPm.AtXD0g5ew2');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (14,'LabA4','Cane','1980-01-16','LABA','14','888-888-8888','la4@g.com','M','$2y$10$3ORPgmcU7366t7wvsQzX.edrKgS7sYcTPx23vLWFiETbXdl3n.2vi');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (15,'Staff2','Hilton','1980-01-16','STFF','15','888-888-8888','s2@g.com','F','$2y$10$dK82nm7QRA8hl2HRYl/YpOzDOYGPBLf5p4Ti4aVTzdgKQOq14CQQq');
INSERT INTO `person` (`person_id`,`person_first_name`,`person_last_name`,`person_DOB`,`person_type`,`person_addr_id`,`person_phone`,`person_email`,`person_gender`,`password`) VALUES (16,'Staff3','Khan','1980-01-16','STFF','16','888-888-8888','s3@g.com','M','$2y$10$h4aS0kkoolvtGTuAWGjoU.PjETkG71flYsYghl9dM0dC5ikmVfhAu');



INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (1,1,'1795 Sams Rd','Apt 222','UTA','Texas',77777,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (1,2,'333 Gone Way','Apt 122','UTA','Texas',88888,'USA',0);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (2,1,'123 Arlington Rd','Apt 100','Arlington Heights','Illinois',88888,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (3,1,'234 McKinney Blvd','Apt 222','Schaumburg','Illinois',67676,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (4,1,'312 Roosevelt Ave','103','Schaumburg','Illinois',67676,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (5,1,'2233 Lake Cood Dr','111','Deerpark','Illinois',60005,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (6,1,'5555 Farm to Market','423','Oak Brook','Illinois',60587,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (7,1,'5000 Lone Star Pkwy',NULL,'Frisco','Texas',75034,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (8,1,'2800 Midwest Rd',NULL,'Oak Brook','Illinois',60523,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (9,1,'134 E Lake St','222','Bloomingdale','Illinois',60108,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (10,1,'930 S Roselle Rd','212','Schaumburg','Illinois',60193,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (11,1,'678 Wilke Rd','234','Deerpark','Illinois',67890,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (12,1,'982 S BarringtonRd','222','Barrington','Illinois',60009,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (13,1,'678 McHenry Rd','22','Irving','Illinois',60009,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (14,1,'789 Overland Park','212','Overland Park','Illinois',78892,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (15,1,'969 E Maple Ave','889','Roselle','Illinois',60172,'USA',1);
INSERT INTO `person_address` (`person_id`,`address_code`,`address_line1`,`address_line2`,`address_city`,`address_state`,`address_zip`,`address_country`,`address_correspondence`) VALUES (16,1,'72 S Prospect Rd','21','Roselle','Illinois',60712,'USA',1);



INSERT INTO `staff` (`staff_id`,`DOJ`,`qualification`) VALUES (3,'2014-01-01','BS');
INSERT INTO `staff` (`staff_id`,`DOJ`,`qualification`) VALUES (15,'2013-05-05','BS');
INSERT INTO `staff` (`staff_id`,`DOJ`,`qualification`) VALUES (16,'2010-05-05','MS');

INSERT INTO `patient` (`patient_id`,`patient_DOR`,`insurance_id`) VALUES (2,'2012-10-11',NULL);
INSERT INTO `patient` (`patient_id`,`patient_DOR`,`insurance_id`) VALUES (7,'2013-10-11',NULL);
INSERT INTO `patient` (`patient_id`,`patient_DOR`,`insurance_id`) VALUES (8,'2010-10-11',NULL);
INSERT INTO `patient` (`patient_id`,`patient_DOR`,`insurance_id`) VALUES (9,'2011-10-11',NULL);
INSERT INTO `patient` (`patient_id`,`patient_DOR`,`insurance_id`) VALUES (10,'2012-10-11',NULL);

INSERT INTO `department` (`department_id`,`department_name`,`department_location`,`department_head`) VALUES (1,'Ortho','401 North Street',null);
INSERT INTO `department` (`department_id`,`department_name`,`department_location`,`department_head`) VALUES (2,'Pediatrics','402 North Street',null);
INSERT INTO `department` (`department_id`,`department_name`,`department_location`,`department_head`) VALUES (3,'Internal Medicine','403 North St',null);

INSERT INTO `availability_group` (`availability_group`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES ('MT',1,1,0,0,0,0,0);
INSERT INTO `availability_group` (`availability_group`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES ('MTW',1,1,1,0,0,0,0);
INSERT INTO `availability_group` (`availability_group`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES ('MTWTH',1,1,1,1,0,0,0);
INSERT INTO `availability_group` (`availability_group`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES ('TWTH',0,1,1,1,0,0,0);
INSERT INTO `availability_group` (`availability_group`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES ('WKDY',1,1,1,1,1,0,0);
INSERT INTO `availability_group` (`availability_group`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES ('WKND',0,0,0,0,0,1,1);


INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('WKND','08:00:00','17:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('WKND','17:00:00','00:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('WKND','00:00:00','08:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MTW','08:00:00','17:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MTW','17:00:00','00:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MTW','00:00:00','08:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('WKDY','08:00:00','17:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('WKDY','17:00:00','00:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('WKDY','00:00:00','08:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MT','08:00:00','17:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MT','17:00:00','00:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MT','00:00:00','08:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('TWTH','08:00:00','17:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('TWTH','17:00:00','00:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('TWTH','00:00:00','08:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MTWTH','08:00:00','17:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MTWTH','17:00:00','00:00:00');
INSERT INTO `availability` (`availability_group`,`availability_start_time`,`availability_end_time`) VALUES ('MTWTH','00:00:00','08:00:00');


INSERT INTO `doctor` (`doctor_id`,`department_id`,`doctor_DOJ`,`qualification`,`availability_id`,`association`,`designation`,`fees`) VALUES (1,1,'2010-11-24','MD',1,'Medical Association','HOD Ortho','150.00');
INSERT INTO `doctor` (`doctor_id`,`department_id`,`doctor_DOJ`,`qualification`,`availability_id`,`association`,`designation`,`fees`) VALUES (11,2,'2010-07-07','MD Pediatrics',11,'Pediatric Association','HOD Pediatrics','120.00');
INSERT INTO `doctor` (`doctor_id`,`department_id`,`doctor_DOJ`,`qualification`,`availability_id`,`association`,`designation`,`fees`) VALUES (12,3,'2009-11-24','MD',12,'Medical Association','HOD Internal Medicine','120.00');
INSERT INTO `doctor` (`doctor_id`,`department_id`,`doctor_DOJ`,`qualification`,`availability_id`,`association`,`designation`,`fees`) VALUES (13,2,'2011-11-30','Pediatrics',13,'Pediatric Association','Senior Doctor','120.00');

UPDATE `department` SET `department_head`=1 WHERE `department_id`='1';
UPDATE `department` SET `department_head`=11 WHERE `department_id`='2';
UPDATE `department` SET `department_head`=12 WHERE `department_id`='3';

INSERT INTO `lab_assistant` (`lab_assistant_id`,`DOJ`,`assistant_qualification`,`incharge_id`) VALUES (4,'2013-05-07','BS',NULL);
INSERT INTO `lab_assistant` (`lab_assistant_id`,`DOJ`,`assistant_qualification`,`incharge_id`) VALUES (5,'2010-11-24','BS',4);
INSERT INTO `lab_assistant` (`lab_assistant_id`,`DOJ`,`assistant_qualification`,`incharge_id`) VALUES (6,'2014-11-24','MS',4);
INSERT INTO `lab_assistant` (`lab_assistant_id`,`DOJ`,`assistant_qualification`,`incharge_id`) VALUES (14,'2013-11-24','MS',4);



INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (1,'United Health Care','UHC 500','35.00','John',2);
INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (2,'Blue Cross Blue Shield','BCBS 300','50.00','Mary',7);
INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (3,'United Health Care','UHC PPO','100.00','George',8);
INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (4,'Blue Cross Blue Shield','BCBS PPO','150.00','Sam',9);
INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (5,'Land of Lincoln','LL 150','150.00','John',2);
INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (6,'United Health Care','UHC 200','100.00','Peter',10);
INSERT INTO `insurance` (`insurance_id`,`insurance_name`,`insurance_plan`,`insurance_copay`,`insurance_primary`,`patient_id`) VALUES (7,'Blue Cross Blue Shield','BCBS 300','50.00','Bob',7);



INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (1,'Albumin Test','100.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (2,'Allergy Test','120.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (3,'Blood Glucose','50.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (4,'Blood Type','30.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (5,'Cholestrol','40.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (6,'Complete Blood Count','59.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (7,'Pregnancy Test','40.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (8,'Rubella','58.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (9,'Sickle Cell Test','80.00');
INSERT INTO `laboratory_test` (`test_id`,`test_name`,`test_charge`) VALUES (10,'Viral Test','80.00');


INSERT INTO `laboratory_report` (`report_id`,`patient_id`,`doctor_id`,`test_date`,`test_id`,`status`,`test_result_date`,`test_result`,`test_result_details`) VALUES (1,2,1,'2015-09-24 16:00:00',2,'O','2015-09-26 16:00:00','Positive','Has allergy');
INSERT INTO `laboratory_report` (`report_id`,`patient_id`,`doctor_id`,`test_date`,`test_id`,`status`,`test_result_date`,`test_result`,`test_result_details`) VALUES (2,2,12,'2016-02-12 16:00:00',3,'O','2016-02-15 16:00:00','Positive','Have issue');
INSERT INTO `laboratory_report` (`report_id`,`patient_id`,`doctor_id`,`test_date`,`test_id`,`status`,`test_result_date`,`test_result`,`test_result_details`) VALUES (3,7,11,'2015-04-09 14:00:00',4,'O','2015-04-11 14:00:00','Negative','No issue');
INSERT INTO `laboratory_report` (`report_id`,`patient_id`,`doctor_id`,`test_date`,`test_id`,`status`,`test_result_date`,`test_result`,`test_result_details`) VALUES (4,8,11,'2015-04-09 12:00:00',9,'C','2015-04-11 12:00:00','Positive','Yes he has it');



INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (1,3,2,1,'10:00:00','2015-09-24','C');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (2,3,2,1,'09:00:00','2015-05-24','O');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (3,15,7,11,'12:00:00','2015-04-09','O');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (4,15,8,11,'15:00:00','2015-06-16','C');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (5,16,9,12,'15:00:00','2014-05-13','O');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (6,16,9,12,'18:00:00','2014-04-16','O');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (7,15,8,13,'15:00:00','2015-01-06','C');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (8,16,10,1,'11:00:00','2016-01-21','S');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (9,16,8,11,'12:00:00','2016-01-11','S');
INSERT INTO `appointment` (`appointment_id`,`staff_id`,`patient_id`,`doctor_id`,`appt_time`,`appt_day`,`status`) VALUES (10,3,2,12,'07:00:00','2016-02-12','S');


INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (2,1,1,2,'Cough, Fever','Common Cold','Acetaminohen',NULL,NULL,'2015-09-28');
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (2,2,1,3,'Ear pain','Ear infection','Antibiotics',NULL,NULL,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (2,10,12,1,'Continous Cough, Throwing up','GERD','GERD meds',1,2,'2015-12-17');
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (7,3,11,4,NULL,NULL,NULL,9,4,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (8,4,11,4,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (8,7,13,4,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (8,9,11,5,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (9,5,12,5,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (9,6,12,3,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `patient_history` (`patient_id`,`appointment_id`,`doctor_id`,`ailment_id`,`symptoms`,`diagnosis`,`prescription`,`test_id`,`report_id`,`next_visit_date`) VALUES (10,8,1,6,NULL,NULL,NULL,NULL,NULL,NULL);


