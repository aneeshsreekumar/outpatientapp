create database outpatientdb;

use database outpatientdb;

DROP TABLE IF EXISTS person CASCADE ;
DROP TABLE IF EXISTS person_address	CASCADE;

CREATE  TABLE `outpatientdb`.`person` (

  `person_id` INT NOT NULL ,

  `person_first_name` VARCHAR(45) NULL ,

  `person_last_name` VARCHAR(45) NULL ,

  `person_DOB` DATE NULL ,

  `person_type` VARCHAR(45) NULL ,

  `person_addr_id` VARCHAR(45) NULL ,

  `person_phone` VARCHAR(15) NULL ,

  `person_email` VARCHAR(45) NULL ,

  `person_gender` VARCHAR(1) NULL ,

  PRIMARY KEY (`person_id`) );

 
  
  CREATE  TABLE `outpatientdb`.`person_address` (

  `person_id` INT NOT NULL ,

  `address_code` INT NOT NULL ,

  `address_line1` VARCHAR(45) NULL ,

  `address_line2` VARCHAR(45) NULL ,

  `address_city` VARCHAR(45) NULL ,

  `address_state` VARCHAR(45) NULL ,

  `address_zip` INT NULL ,

  `address_country` VARCHAR(30) NULL ,

  `address_correspondence` TINYINT(1)  NULL ,

  PRIMARY KEY (`person_id`, `address_code`) );


ALTER TABLE `outpatientdb`.`person_address` 

  ADD CONSTRAINT `PersonAddress_FK1`

  FOREIGN KEY (`person_id` )

  REFERENCES `outpatientdb`.`person` (`person_id` )

  ON DELETE CASCADE

  ON UPDATE CASCADE

, ADD INDEX `PersonAddress_FK1` (`person_id` ASC) ;


create table user_role (
	role_type varchar(2) not null,
	role_description varchar(20) not null,
	primary key(role_type)
);

create table user_t (
		user_login			varchar(15)         not null,
        first_name             varchar(40)         not null,
        last_name             varchar(40)         not null,
        password           varchar(255) not null,
        user_email			varchar(40),
        role_type			varchar(2) not null,
        primary key (user_login),
        FOREIGN KEY (role_type) REFERENCES user_role(role_type)    
) ;



ALTER TABLE `outpatientdb`.`patient_history` 

  ADD CONSTRAINT `PatientHistory_FK1`

  FOREIGN KEY (`patient_id` )

  REFERENCES `outpatientdb`.`patient` (`patient_id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `PatientHistory_FK2`

  FOREIGN KEY (`appointment_id` )

  REFERENCES `outpatientdb`.`appointment` (`appointment_id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `PatientHistory_FK3`

  FOREIGN KEY (`doctor_id` )

  REFERENCES `outpatientdb`.`doctor` (`doctor_id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `PatientHistory_FK4`

  FOREIGN KEY (`ailment_id` )

  REFERENCES `outpatientdb`.`ailment` (`ailment_code` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `PatientHistory_FK5`

  FOREIGN KEY (`test_code` )

  REFERENCES `outpatientdb`.`laboratory_test` (`test_code` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `PatientHistory_FK6`

  FOREIGN KEY (`report_id` )

  REFERENCES `outpatientdb`.`laboratory_report` (`report_id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `PatientHistory_FK1` (`patient_id` ASC) 

, ADD INDEX `PatientHistory_FK2` (`appointment_id` ASC) 

, ADD INDEX `PatientHistory_FK3` (`doctor_id` ASC) 

, ADD INDEX `PatientHistory_FK4` (`ailment_id` ASC) 

, ADD INDEX `PatientHistory_FK5` (`test_code` ASC) 

, ADD INDEX `PatientHistory_FK6` (`report_id` ASC) ;



-- inserts

INSERT INTO user_role  (role_type, role_description)
	VALUES  ('P', 'Patient');
INSERT INTO user_role  (role_type, role_description)
	VALUES  ('S', 'Staff');
INSERT INTO user_role  (role_type, role_description)
	VALUES  ('D', 'Doctor');
INSERT INTO user_role  (role_type, role_description)
	VALUES  ('H', 'Pharmacist');



