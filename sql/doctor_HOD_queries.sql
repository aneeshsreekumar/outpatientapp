-- doctor wants to see his/her pending appointments on a given day and kind of ailments with patient name and gender

select p.person_first_name, p.person_last_name, p.person_gender, al.ailment_description, appt_time 
from appointment ap, ailment al, person p
where ap.patient_id = p.person_id
and ap.ailment_id = al.ailment_id
and ap.doctor_id = 12
and ap.appt_day = date(now())
and ap.appt_time > time(now())
and ap.status = 'SCH';


-- HOD wants to see how many reports were ordered by which doctor on a given day

select d.doctor_id, concat(p.person_first_name,' ',p.person_last_name) as 'Doctor Name', count(lr.report_id) as inProcess_report_count
from 
laboratory_report lr,
doctor d,
person p
where d.doctor_id = p.person_id
and d.doctor_id = lr.doctor_id
and date(lr.test_date) = date(now())
group by d.doctor_id, concat(p.person_first_name,' ',p.person_last_name);


-- HOD wants to see how many reports were ordered by each doctor and how many are in process
select doctor_id, doctor_name, 
max(case when count_type = 'ALL' then report_count else null end) as All_report_count,
max(case when count_type = 'IN PROCESS' then report_count else null end) as in_process_report_count
from (
select d.doctor_id, concat(p.person_first_name,' ',p.person_last_name) as doctor_name, 'ALL' as count_type, count(lr.report_id) as report_count
from 
laboratory_report lr,
doctor d,
person p
where d.doctor_id = p.person_id
and d.doctor_id = lr.doctor_id
and date(lr.test_date) = date(now())
group by d.doctor_id, concat(p.person_first_name,' ',p.person_last_name)

union

select d.doctor_id, concat(p.person_first_name,' ',p.person_last_name) as 'Doctor Name', 'IN PROCESS' as count_type, count(lr.report_id) as report_count
from 
laboratory_report lr,
doctor d,
person p
where d.doctor_id = p.person_id
and d.doctor_id = lr.doctor_id
and date(lr.test_date) = date(now())
and lr.status = 'INP'
group by d.doctor_id, concat(p.person_first_name,' ',p.person_last_name)

) as tab
group by doctor_id, doctor_name;

-- correlated query to see how many new patients a doctor is seeing today

select * from appointment a where not exists
(select * from patient_history ph where ph.patient_id = a.patient_id) and appt_day = date(now());

