-- for simiplicity assumptions are -
-- 1. appointments are offered no more than a week in advance
-- 2. timetable is refreshed weekly by a scheduled event

-- query for staff to view the appointments
-- it will have buttons to create new appointment
-- cancel an appointment, reschedule an appointment
select a.appointment_id, concat(patient.person_first_name,' ', patient.person_last_name) as patient_name, 
concat(doctor.person_first_name,' ', doctor.person_last_name) as doctor_name, a.appt_time, a.appt_day, a.status
from appointment a, person as patient, person as doctor
where a.patient_id = patient.person_id
and a.doctor_id = doctor.person_id
and appt_day = date(now())
order by appt_time;

-- insert query for new appointment
insert into appointment(patient_id, staff_id, doctor_id, appt_time, appt_day, status)
values(:patient_id, :staff_id, :doctor_id, :appt_time, :appt_day, 'SCH');

-- update query for cancellation of an appointment
update appointment
set status = 'CAN'
where appointment_id = :appointment_id;

-- update query for re-scheduling of an appointment
update appointment
set appt_day = :appt_day,
 appt_time = :appt_time
where appointment_id = :appointment_id;

-- doctor high-level availability check
	select concat(p.person_first_name, ' ', p.person_last_name) as doctor_name, 
		a.availability_group, availability_start_time, availability_end_time
	from doctor d, availability a, person p
	where d.doctor_id = p.person_id
	and d.availability_id = a.availability_id;

-- total fees for a patient with a given appointment
select a.patient_id, 
	sum(fees + test_charge) as total_due, 
    if(insurance_copay is null, fees + test_charge, (fees + test_charge) - insurance_copay) as due_now,
    d.fees as doctor_fees, 
    lt.test_charge, 
    ifnull(i.insurance_copay, 0) as insurance_copay
from appointment a join doctor d
on a.doctor_id = d.doctor_id

join patient_history ph
on a.appointment_id = ph.appointment_id

join patient p
on a.patient_id = p.patient_id

left outer join laboratory_test lt
on ph.test_id = lt.test_id

left outer join insurance i
on p.insurance_id = i.insurance_id

where a.appointment_id = :appointment_id;


-- fetch day availability
select d.doctor_id, ag.availability_group 
from doctor d join availability a
on d.availability_id = a.availability_id

join availability_group ag
on a.availability_group = ag.availability_group

where doctor_id = :doctor_id;

-- fetch time availability
select :day, timeslot from timetable
where doctor_id = 2
and :day <> 0;


-- event to refresh timetable every week sunday night
create event evnet_refresh_timetable
	on schedule 
		every 1 week
        starts '2015-11-29 00:00:00' 
    do call proc_generate_timetable_for_all();

    
-- procedure for calling refresh timetable proc for each doctor
delimiter //
drop procedure if exists proc_generate_timetable_for_all//
create procedure proc_generate_timetable_for_all()
begin
	-- declare variables/cursor
    declare current_doctor_id int(11);
    declare done int default 0;
    declare cur_doctor cursor for select doctor_id from doctor;
    DECLARE CONTINUE HANDLER FOR NOT FOUND set done = 1;
    
    open cur_doctor;
    
    update_loop: loop
		if done = 1 then 
			leave update_loop; 
		end if;
		fetch cur_doctor into current_doctor_id;
        call generate_timetable(current_doctor_id);
    end loop;

	close cur_doctor;
    
end //
delimiter ;



-- procedure for generating a time-table to be viewed by staff or HOD

DELIMITER //
drop procedure if exists generate_timetable //
create procedure generate_timetable(IN d_id int(11))
BEGIN
	
	-- declare variables
	declare a_id int;
    declare a_grp varchar(10);
    declare start_time, end_time time;
    declare time_interval time default '01:00:00';
    declare timeslot time;
    declare m_flag, t_flag, w_flag, th_flag, f_flag, s_flag, sn_flag tinyint(1);
    
    -- fetch availability info based on availability id  
	select d.availability_id, availability_start_time, availability_end_time, availability_group
	into a_id, start_time, end_time, a_grp
	from doctor d, availability a
	where d.availability_id = a.availability_id
		and doctor_id = d_id;
    
	-- fetch working days info based on group
    select monday, tuesday, wednesday, thursday, friday, saturday, sunday
    into m_flag, t_flag, w_flag, th_flag, f_flag, s_flag, sn_flag
    from availability_group 
    where availability_group = a_grp;
	
	-- refresh the timetable for the given doctor
    delete from timetable where doctor_id = d_id;
    
    -- set timeslot start point
    set timeslot = start_time;
    
    -- insert entries into the time table
	while timeslot <= end_time do
		insert into timetable 
        values(d_id, timeslot, m_flag, t_flag, w_flag, th_flag, f_flag, s_flag, sn_flag);
        set timeslot = timeslot + time_interval;
        -- set counter = counter + 1;
    end while;

END //
DELIMITER ;

-- trigger to refresh timetable in case of update to doctor
delimiter //
drop trigger if exists refresh_timetable //
create trigger refresh_timetable 
	after update on doctor
    for each row
begin
    if ((NEW.availability_id <> OLD.availability_id) and (NEW.availability_id <> '')) then
		call generate_timetable(OLD.doctor_id);
    end if;
end //
delimiter ;

-- trigger to update the timetable in case of insert of a new doctor

delimiter //
drop trigger if exists update_timetable //
create trigger update_timetable 
	after insert on doctor
    for each row
begin
	call generate_timetable(NEW.doctor_id);
end //
delimiter ;

-- procedure to update timetable for a new/change appointment

DELIMITER //
drop procedure if exists proc_refresh_timetable //
create procedure proc_refresh_timetable(IN a_id int(11), IN flag varchar(5))
BEGIN
	
	-- declare variables
    declare d_id int(11);
    declare a_time time;
    declare a_day tinyint(1);
    declare booked_flag tinyint(1);
    -- declare sqlstring varchar(100);
    
    -- fetch appointment info based on appointment id  
	select doctor_id, appt_time, dayofweek(appt_day)
	into d_id, a_time, a_day
	from appointment
	where appointment_id = a_id;
    
    if (flag = 'reset') then
		set booked_flag = 1;
	elseif(flag = 'set') then
		set booked_flag = 0;
	end if;
    
    case
		when a_day = 1
			then 
            update timetable set sunday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 2
			then 
            update timetable set monday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 3
			then 
            update timetable set tuesday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 4
			then 
            update timetable set wednesday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 5
			then 
            update timetable set thursday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 6
			then 
            update timetable set friday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 7
			then 
            update timetable set saturday = booked_flag where doctor_id = d_id and timeslot = a_time;
	end case;
END //
DELIMITER ;

-- trigger to update timetable if new appointment is created
delimiter //
drop trigger if exists appt_instrig_update_timetable//
create trigger appt_instrig_update_timetable
	after insert on appointment
    for each row
begin
	call proc_ins_patient_history(NEW.appointment_id);
	call proc_refresh_timetable(NEW.appointment_id, 'set');
end //
delimiter ;

-- trigger to update timetable if an appointment is re-scheduled
delimiter //
drop trigger if exists appt_befupdtrig_update_timetable//
create trigger appt_befupdtrig_update_timetable
	before update on appointment
    for each row
begin
	call proc_refresh_timetable(OLD.appointment_id, 'reset');
end //
delimiter ;

-- trigger to update timetable if an appointment is re-scheduled
delimiter //
drop trigger if exists appt_aftupdtrig_update_timetable//
create trigger appt_aftupdtrig_update_timetable
	after update on appointment
    for each row
begin
	if ((NEW.appt_time <> OLD.appt_time) or (NEW.appt_day <> OLD.appt_day)) then
		call proc_refresh_timetable(OLD.appointment_id, 'set');
    end if;
end //
delimiter ;

-- procedure for inserting record in to patient_history table when appointment is created
delimiter //
drop procedure if exists proc_ins_patient_history//
create procedure proc_ins_patient_history(IN a_id int(11))
begin
	-- declare variables
    declare p_id, d_id, al_id int(11);
    
    -- fetch the latest appointment record
    select patient_id, doctor_id, ailment_id
    into p_id, d_id, al_id
    from appointment
    where appointment_id = a_id;
    
    insert into patient_history(patient_id, appointment_id, doctor_id, ailment_id)
    values(p_id, a_id, d_id, al_id);
    
end //
delimiter ;

-- check out the new tests in queue

select concat(d.person_first_name, ' ', d.person_last_name) as doctor_name, concat(p.person_first_name, ' ', p.person_last_name) as patient_name, test_name, appt_day
from patient_history ph join person p
on ph.patient_id = p.person_id

join person d
on ph.doctor_id = d.person_id

join appointment a
on ph.appointment_id = a.appointment_id

join laboratory_test lt
on ph.test_id = lt.test_id

where (ph.test_id is not null) and (ph.report_id is null);

-- more than one report ordered for same test
select lr.test_id, lt.test_name, count(lr.test_id) as test_count
from laboratory_report lr join laboratory_test lt
on lr.test_id = lt.test_id
where lower(lr.status) = 'inp'
group by lr.test_id, lt.test_name
having count(*)>1;


-- reports lying pending for more than 2 days
select lr.test_id, lt.test_name
from laboratory_report lr join laboratory_test lt
on lr.test_id = lt.test_id
where lower(lr.status) = 'inp'
and datediff(date(now()), test_date) > 1


-- turnaround time for reports with more than one person working on them
select lr.report_id, datediff(lr.test_result_date, lr.test_date) turnaround_time
from laboratory_report lr

join lab_assignment la
on lr.report_id = la.report_id

group by report_id
having count(la.lab_assistant_id) > 1


-- average time comparison if more than one assistant is working compared to only one workin
select id, avg(turnaround_time) avg_time from 
(
select lr.report_id, 'more than one assistant'as id, datediff(lr.test_result_date, lr.test_date) turnaround_time
from laboratory_report lr

join lab_assignment la
on lr.report_id = la.report_id

group by report_id
having count(la.lab_assistant_id) > 1

union

select lr.report_id, 'one assistant' as id, datediff(lr.test_result_date, lr.test_date) turnaround_time
from laboratory_report lr

join lab_assignment la
on lr.report_id = la.report_id

group by report_id
having count(la.lab_assistant_id) = 1
) t
group by id;


-- doctor searches for patients symptoms over the visits in ascending order

select a.appt_day, ph.symptoms, ph.diagnosis, ph.prescription, lt.test_name, lr.test_result
from patient_history ph

join appointment a
on ph.appointment_id = a.appointment_id

left outer join laboratory_test lt
on ph.test_id = lt.test_id

left outer join laboratory_report lr
on ph.report_id = lr.report_id

order by appt_day;

-- get next patient
select a.patient_id, concat(p.person_first_name, ' ', p.person_last_name) as patient_name, a.appt_time, al.ailment_description
from appointment a

join ailment al
on a.ailment_id = al.ailment_id

join person p
on a.patient_id = p.person_id

join patient_history ph
on a.patient_id = ph.patient_id

where a.doctor_id = :doctor_id
and a.status = "SCH";


-- update patient_history
update patient_history
set symptoms = :symptoms, diagnosis = :diagnosis, prescription = :prescription, test_id = :test_id
where appointment_id = :appointment_id;

-- which doctor ordered maximum number of atype of test and the department to which he/she belongs
select tab.doctor_id, concat(p.person_first_name,' ', p.person_last_name) as doctor_name, department_name, test_cnt, test_name
from
(select doctor_id, test_id, count(*) test_cnt
from patient_history ph
group by doctor_id, test_id
having (test_id, test_cnt) in 
(select test_id, max(test_cnt) max_cnt from 
	(select doctor_id, test_id, count(*) as test_cnt
	from patient_history ph
	group by doctor_id, test_id) t
    group by test_id
)) tab
join person p
on tab.doctor_id = p.person_id

join laboratory_test lt
on tab.test_id = lt.test_id

join doctor d
on tab.doctor_id = d.doctor_id

join department dep
on d.department_id = dep.department_id;


-- report status for a patient, search by patient last name
select concat(p.person_first_name, " ", p.person_last_name), p.person_DOB, p.person_gender,
	lt.test_name, ph.report_id, lr.status
from person p

join patient_history ph
on p.person_id = ph.patient_id

join laboratory_test lt
on ph.test_id = lt.test_id

join laboratory_report lr
on ph.report_id = lr.report_id

where lower(p.person_last_name) like concat('%', :lastname,'%')
order by lr.test_date desc;

-- get reports currently in process

select lr.patient_id, concat(p.person_first_name, " ", p.person_last_name), lr.report_id,  lr.test_date, lt.test_id, lt.test_name, lr.status
from laboratory_report lr

join laboratory_test lt
on lr.test_id = lt.test_id

join person p
on lr.patient_id = p.person_id

where lower(status) = 'inp';

-- see a patient's report by last name search
select lr.patient_id, concat(p.person_first_name, " ", p.person_last_name), lr.report_id,  lt.test_id, lt.test_name, lr.status, 
	p.person_DOB, p.person_gender, lr.test_result, lr.test_result_date, lr.test_result_details
from person p

join patient_history ph
on p.person_id = ph.patient_id

join laboratory_test lt
on ph.test_id = lt.test_id

join laboratory_report lr
on ph.report_id = lr.report_id

where lower(p.person_last_name) like concat('%', :lastname,'%')
order by test_date desc;

-- update a report status
update laboratory_report
set test_result_date = now(),
test_result = :result,
test_result_details = :details,
status = 'COM'
where report_id = :report_id;

-- insert into lab_assistant
insert into lab_assignment values(:lab_assistant_id, :report_id);


