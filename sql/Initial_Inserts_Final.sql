insert into ailment values(1,'GERD');
insert into ailment values(2,'Common Cold');
insert into ailment values(3,'ENT');
insert into ailment values(4,'Stomach');
insert into ailment values(5,'Allergy');
insert into ailment values(6,'Asthma');
insert into ailment values(7,'Flu');
insert into ailment values(8,'Joint Pain');
insert into ailment values(9,'Arthiritis');


insert into person_type values('DOCT','Doctor',now());
insert into person_type values('STFF','Staff',now());
insert into person_type values('PTNT','Patient',now());
insert into person_type values('LABA','Lab Assistant',now());
insert into person_type values('PHRM','Pharmacist',now());

insert into insurance values(1,'United Health Care','UHC 500',35);
insert into insurance values(2,'Blue Cross Blue Shield','BCBS 300',50);
insert into insurance values(3,'United Health Care','UHC PPO',75);
insert into insurance values(4,'Blue Cross Blue Shield','BCBS PPO',15);
insert into insurance values(5,'Land of Lincoln','LL 150',15);
insert into insurance values(6,'United Health Care','UHC 200',20);
insert into insurance values(7,'Blue Cross Blue Shield','BCBS 300',25);


insert into laboratory_test values(1,'Albumin Test',100);
insert into laboratory_test values(2,'Allergy Test',120);
insert into laboratory_test values(3,'Blood Glucose',50);
insert into laboratory_test values(4,'Blood Test',30);
insert into laboratory_test values(5,'Cholestrol',40);
insert into laboratory_test values(6,'Complete Blood Count',50);
insert into laboratory_test values(7,'Pregnancy Test',40);
insert into laboratory_test values(8,'Rubella',58);
insert into laboratory_test values(9,'Sickle Cell Test',80);
insert into laboratory_test values(10,'Viral Test',80);


insert into availability_group values('MTWT',1,1,1,1,0,0,0);
insert into availability_group values('WKND',0,0,0,0,1,1,1);
insert into availability_group values('WKDY',1,1,1,1,1,0,0);
insert into availability_group values('NONE',0,0,0,0,0,0,0);
insert into availability_group values('TWTF',0,1,1,1,1,0,0);
insert into availability_group values('TWTh',0,1,1,1,0,0,0);
insert into availability_group values('MWTh',1,0,1,1,0,0,0);
insert into availability_group values('WThF',0,0,1,1,1,0,0);
insert into availability_group values('SSuM',1,0,0,0,0,1,1);
insert into availability_group values('MWFr',1,0,1,0,1,0,0);


insert into person values(1001,'Patient1','Sick','1990-01-23','PTNT',1,NULL,'p1@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1002,'Patient2','Healthy','1995-04-04','PTNT',1,NULL,'p2@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1003,'Patient3','Ghi','1983-07-06','PTNT',1,NULL,'p3@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1004,'Patient4','Jkl','1961-12-31','PTNT',1,NULL,'p4@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1005,'Patient5','Mno','1947-08-22','PTNT',1,NULL,'p5@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1006,'Patient6','Pqr','1984-10-03','PTNT',1,NULL,'p6@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1007,'Patient7','Stu','2012-03-05','PTNT',1,NULL,'p7@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1008,'Patient8','Vwx','2014-11-20','PTNT',1,NULL,'p8@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1009,'Patient9','Yz','2014-05-15','PTNT',1,NULL,'p9@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(1010,'Patient10','Often','2013-03-19','PTNT',1,NULL,'p10@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(2001,'Staff1','Bcd','1980-06-12','STFF',1,NULL,'s1@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(2002,'Staff2','Efg','1975-02-15','STFF',1,NULL,'s2@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(3001,'LabAssistant1','Hij','1986-02-16','LABA',1,NULL,'l1@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(3002,'LabAssistant2','Klo','1988-04-29','LABA',1,NULL,'l2@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(4001,'Doctor1','Abcd','1966-03-12','DOCT',1,NULL,'d1@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(4002,'Doctor2','Qwer','1970-09-09','DOCT',1,NULL,'d2@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(4003,'Doctor3','Asdf','1975-06-23','DOCT',1,NULL,'d3@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(4004,'Doctor4','Poil','1979-10-21','DOCT',1,NULL,'d4@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(4005,'Doctor5','Trew','1980-09-02','DOCT',1,NULL,'d5@email.com','M','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');
insert into person values(4006,'Doctor6','Loki','1982-11-11','DOCT',1,NULL,'d6@email.com','F','$2y$10$hr5YLU27I5evCZMZJuIIr.XhwgJUd9sBPFMktOrO4EoodHIXeiJgy');


insert into patient values(1001,'2015-01-01',4);
insert into patient values(1002,'2015-01-01',1);
insert into patient values(1003,'2015-01-01',4);
insert into patient values(1004,'2015-01-01',1);
insert into patient values(1005,'2015-01-01',2);
insert into patient values(1006,'2015-01-01',4);
insert into patient values(1007,'2015-01-01',4);
insert into patient values(1008,'2015-01-01',6);
insert into patient values(1009,'2015-01-01',7);
insert into patient values(1010,'2015-01-01',5);

insert into availability values(1,'MTWT','09:00:00','17:00:00');
insert into availability values(2,'WKND','09:00:00','17:00:00');
insert into availability values(3,'WKDY','09:00:00','17:00:00');
insert into availability values(4,'NONE','09:00:00','17:00:00');
insert into availability values(5,'TWTF','09:00:00','17:00:00');
insert into availability values(6,'TWTh','09:00:00','17:00:00');
insert into availability values(7,'MWTh','09:00:00','17:00:00');
insert into availability values(8,'WThF','09:00:00','17:00:00');
insert into availability values(9,'SSuM','09:00:00','17:00:00');
insert into availability values(10,'MWFr','09:00:00','17:00:00');

insert into department(department_id, department_name, department_location) values(1,'Pediatrics','1st Floor');
insert into department(department_id, department_name, department_location) values(2,'Ortho','2nd Floor');
insert into department(department_id, department_name, department_location) values(3,'Internal Medicine','3rd Floor');

insert into doctor values(4001,1,'2014-01-01','MD',1,'NAMA','Senior',75);
insert into doctor values(4002,1,'2014-01-01','MBBS',3,'NAMA','HOD',100);
insert into doctor values(4003,2,'2014-01-01','MD',1,'EMA','Junior',50);
insert into doctor values(4004,2,'2014-01-01','MBBS',3,'NAMA','HOD',100);
insert into doctor values(4005,3,'2014-01-01','MD',3,'NAMA','Senior',100);
insert into doctor values(4006,3,'2014-01-01','MD',3,'EMA','HOD',100);


insert into staff values(2001,'2014-01-01');
insert into staff values(2002,'2013-05-05');


insert into lab_assistant values(3001,'2010-11-24','MS',NULL);
insert into lab_assistant values(3002,'2014-11-24','MS',3001);

insert into appointment values(1,2001,1001,4005,'09:00:00','2015-09-05','COM',2);
insert into appointment values(2,2001,1002,4005,'10:00:00','2015-11-01','COM',3);
insert into appointment values(3,2001,1003,4004,'13:00:00','2015-11-03','COM',4);
insert into appointment values(4,2002,1004,4004,'14:00:00','2015-11-04','COM',5);
insert into appointment values(5,2001,1005,4006,'11:00:00','2015-11-05','COM',7);
insert into appointment values(6,2002,1006,4002,'15:00:00','2015-11-01','COM',8);
insert into appointment values(7,2001,1007,4001,'16:00:00','2015-11-13','COM',9);
insert into appointment values(8,2001,1008,4006,'09:00:00','2015-11-13','COM',2);
insert into appointment values(9,2002,1009,4003,'14:00:00','2015-11-15','COM',3);
insert into appointment values(10,2001,1001,4005,'11:00:00','2015-10-10','COM',4);
insert into appointment values(11,2001,1010,4002,'14:00:00','2015-10-01','COM',5);
insert into appointment values(12,2001,1003,4003,'11:00:00','2015-11-15','COM',7);
insert into appointment values(13,2002,1004,4004,'15:00:00','2015-11-24','COM',2);
insert into appointment values(14,2002,1004,4001,'16:00:00','2015-11-23','COM',3);
insert into appointment values(15,2001,1001,4005,'09:00:00','2015-10-30','COM',4);
insert into appointment values(16,2002,1001,4005,'16:00:00','2015-11-20','COM',7);
insert into appointment values(17,2002,1005,4001,'10:00:00','2015-11-02','COM',2);
insert into appointment values(18,2002,1009,4003,'15:00:00','2015-11-02','COM',3);
insert into appointment values(19,2002,1010,4005,'15:00:00','2015-11-26','COM',4);


insert into laboratory_report values(1,1002,4005,'2015-11-01',10,'DLV','2015-11-02','NEGATIVE','NEGATIVE');
insert into laboratory_report values(2,1003,4004,'2015-11-03',4,'DLV','2015-11-05','NEGATIVE','NEGATIVE');
insert into laboratory_report values(3,1004,4004,'2015-11-04',4,'DLV','2015-11-05','POSITIVE','POSITIVE');
insert into laboratory_report values(4,1005,4006,'2015-11-05',9,'DLV','2015-11-07','NEGATIVE','NEGATIVE');
insert into laboratory_report values(5,1008,4006,'2015-11-13',10,'DLV','2015-11-15','POSITIVE','POSITIVE');
insert into laboratory_report values(6,1009,4003,'2015-11-15',10,'DLV','2015-11-17','POSITIVE','POSITIVE');
insert into laboratory_report values(7,1001,4005,'2015-10-10',4,'DLV','2015-10-11','NEGATIVE','NEGATIVE');
insert into laboratory_report values(8,1010,4002,'2015-10-01',2,'DLV','2015-10-03','NEGATIVE','NEGATIVE');
insert into laboratory_report values(9,1003,4003,'2015-11-15',3,'DLV','2015-11-16','POSITIVE','POSITIVE');
insert into laboratory_report values(10,1007,4001,'2015-11-23',1,'INP','2015-11-25','POSITIVE','POSITIVE');
insert into laboratory_report values(11,1001,4005,'2015-10-30',9,'INP','2015-10-31','NEGATIVE','NEGATIVE');
insert into laboratory_report values(12,1005,4001,'2015-11-02',4,'DLV','2015-11-03','NEGATIVE','NEGATIVE');
insert into laboratory_report values(13,1010,4005,'2015-11-26',4,'INP','2015-11-27','POSITIVE','POSITIVE');


insert into patient_history values(1001,1,4005,2,'ear pain, cough','ear infection','some medicine',NULL,NULL,NULL);
insert into patient_history values(1002,2,4005,3,'flu','flu','some med 1',10,1,NULL);
insert into patient_history values(1003,3,4004,4,'stomach pain','tbd','some med2',4,2,NULL);
insert into patient_history values(1004,4,4004,5,'joint pain','arthiritis','some med3',4,3,NULL);
insert into patient_history values(1005,5,4006,7,'allergy increase','weather change','new medicine',9,4,NULL);
insert into patient_history values(1006,6,4002,8,'blocked nose','common cold','NULL',NULL,NULL,NULL);
insert into patient_history values(1007,7,4001,9,'normal','normal','NULL',NULL,NULL,NULL);
insert into patient_history values(1008,8,4006,2,'flu','flu','flu shot',10,5,NULL);
insert into patient_history values(1009,9,4003,3,'fever, weakness','viral','viral med',10,6,NULL);
insert into patient_history values(1001,10,4005,4,'chest pain, cough','blood test advised','pain killer',4,7,NULL);
insert into patient_history values(1010,11,4002,5,'swollen hands','allergy','inflammation reducer',2,8,NULL);
insert into patient_history values(1003,12,4003,7,'weakness','diabetes','b12',3,9,NULL);
insert into patient_history values(1004,13,4004,2,'regular','normal','change medicine',NULL,NULL,NULL);
insert into patient_history values(1007,14,4001,3,'lethargic, less appetite','psychological symptoms','fruits and salt',1,10,NULL);
insert into patient_history values(1001,15,4005,4,'chest pain, joint pain in waves','tbd','rest and same continue',9,11,'2015-11-20');
insert into patient_history values(1001,16,4005,7,'less pain in joint but chest pain persist','sickle cell disease','sickclear',NULL,NULL,NULL);
insert into patient_history values(1005,17,4001,2,'somthign','something2','NULL',4,12,NULL);
insert into patient_history values(1009,18,4003,3,'something1, 2','something4','rest',NULL,NULL,NULL);
insert into patient_history values(1010,19,4005,4,'swollen limbs','infection','pain killer, anti-inflammation',4,13,NULL);


insert into lab_assignment values(3001,1);
insert into lab_assignment values(3002,2);
insert into lab_assignment values(3002,3);
insert into lab_assignment values(3001,4);
insert into lab_assignment values(3002,5);
insert into lab_assignment values(3001,6);
insert into lab_assignment values(3002,6);
insert into lab_assignment values(3002,7);
insert into lab_assignment values(3002,8);
insert into lab_assignment values(3001,9);
insert into lab_assignment values(3002,9);
insert into lab_assignment values(3001,10);
insert into lab_assignment values(3001,11);
insert into lab_assignment values(3002,12);
insert into lab_assignment values(3002,13);
insert into lab_assignment values(3001,13);

INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'09:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'10:00:00','0','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'11:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'12:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'13:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'14:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'15:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'16:00:00','0','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4001,'17:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'09:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'10:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'11:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'12:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'13:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'14:00:00','1','1','1','0','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'15:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'16:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4002,'17:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'09:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'10:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'11:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'12:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'13:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'14:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'15:00:00','0','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'16:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4003,'17:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'09:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'10:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'11:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'12:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'13:00:00','1','0','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'14:00:00','1','1','0','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'15:00:00','1','0','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'16:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4004,'17:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'09:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'10:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'11:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'12:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'13:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'14:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'15:00:00','1','1','1','0','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'16:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4005,'17:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'09:00:00','1','1','1','1','0','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'10:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'11:00:00','1','1','1','0','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'12:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'13:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'14:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'15:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'16:00:00','1','1','1','1','1','0','0');
INSERT INTO `timetable` (`doctor_id`,`timeslot`,`monday`,`tuesday`,`wednesday`,`thursday`,`friday`,`saturday`,`sunday`) VALUES (4006,'17:00:00','1','1','1','1','1','0','0');


-- PROCEDURES
-- procedure to refresh the time table weekly for timetable
DELIMITER $$
DROP PROCEDURE IF EXISTS proc_generate_timetable_for_all$$
CREATE PROCEDURE `proc_generate_timetable_for_all`()
begin
	-- declare variables/cursor
    declare current_doctor_id int(11);
    declare done int default 0;
    declare cur_doctor cursor for select doctor_id from doctor;
    DECLARE CONTINUE HANDLER FOR NOT FOUND set done = 1;
    
    open cur_doctor;
    
    update_loop: loop
		if done = 1 then 
			leave update_loop; 
		end if;
		fetch cur_doctor into current_doctor_id;
        call generate_timetable(current_doctor_id);
    end loop;

	close cur_doctor;
    
end$$
DELIMITER ;

-- procedure to refresh the timetable if availability of doctor changes in the doctor table
DELIMITER $$
DROP PROCEDURE IF EXISTS generate_timetable$$
CREATE PROCEDURE `generate_timetable`(IN d_id int(11))
BEGIN
	
	-- declare variables
	declare a_id int;
    declare a_grp varchar(10);
    declare start_time, end_time time;
    declare time_interval time default '01:00:00';
    declare timeslot time;
    declare m_flag, t_flag, w_flag, th_flag, f_flag, s_flag, sn_flag tinyint(1);
    
    -- fetch availability info based on availability id  
	select d.availability_id, availability_start_time, availability_end_time, availability_group
	into a_id, start_time, end_time, a_grp
	from doctor d, availability a
	where d.availability_id = a.availability_id
		and doctor_id = d_id;
    
	-- fetch working days info based on group
    select monday, tuesday, wednesday, thursday, friday, saturday, sunday
    into m_flag, t_flag, w_flag, th_flag, f_flag, s_flag, sn_flag
    from availability_group 
    where availability_group = a_grp;
	
	-- refresh the timetable for the given doctor
    delete from timetable where doctor_id = d_id;
    
    -- set timeslot start point
    set timeslot = start_time;
    
    -- insert entries into the time table
	while timeslot <= end_time do
		insert into timetable 
        values(d_id, timeslot, m_flag, t_flag, w_flag, th_flag, f_flag, s_flag, sn_flag);
        set timeslot = timeslot + time_interval;
        -- set counter = counter + 1;
    end while;

END$$
DELIMITER ;


-- procedure to refresh the timetable if appointment table changes 
DELIMITER $$
DROP PROCEDURE IF EXISTS proc_refresh_timetable$$
CREATE PROCEDURE `proc_refresh_timetable`(IN a_id int(11), IN flag varchar(5))
BEGIN
	
	-- declare variables
    declare d_id int(11);
    declare a_time time;
    declare a_day tinyint(1);
    declare booked_flag tinyint(1);
    -- declare sqlstring varchar(100);
    
    -- fetch appointment info based on appointment id  
	select doctor_id, appt_time, dayofweek(appt_day)
	into d_id, a_time, a_day
	from appointment
	where appointment_id = a_id;
    
    if (flag = 'reset') then
		set booked_flag = 1;
	elseif(flag = 'set') then
		set booked_flag = 0;
	end if;
    
    case
		when a_day = 1
			then 
            update timetable set sunday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 2
			then 
            update timetable set monday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 3
			then 
            update timetable set tuesday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 4
			then 
            update timetable set wednesday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 5
			then 
            update timetable set thursday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 6
			then 
            update timetable set friday = booked_flag where doctor_id = d_id and timeslot = a_time;
		when a_day = 7
			then 
            update timetable set saturday = booked_flag where doctor_id = d_id and timeslot = a_time;
	end case;
END$$
DELIMITER ;


-- TRIGGERS
-- trigger to update timetable if new appointment is created and to add a record to patient_history
delimiter //
drop trigger if exists appt_instrig_update_timetable//
create trigger appt_instrig_update_timetable
	after insert on appointment
    for each row
begin
	call proc_refresh_timetable(NEW.appointment_id, 'set');
end //
delimiter ;

-- trigger to update timetable if an appointment is re-scheduled
delimiter //
drop trigger if exists appt_befupdtrig_update_timetable//
create trigger appt_befupdtrig_update_timetable
	before update on appointment
    for each row
begin
	call proc_refresh_timetable(OLD.appointment_id, 'reset');
end //
delimiter ;

-- trigger to update timetable if an appointment is re-scheduled
delimiter //
drop trigger if exists appt_aftupdtrig_update_timetable//
create trigger appt_aftupdtrig_update_timetable
	after update on appointment
    for each row
begin
	if ((NEW.appt_time <> OLD.appt_time) or (NEW.appt_day <> OLD.appt_day)) then
		call proc_refresh_timetable(OLD.appointment_id, 'set');
    end if;
end //
delimiter ;

-- trigger to update the timetable in case of insert of a new doctor

delimiter //
drop trigger if exists update_timetable //
create trigger update_timetable 
	after insert on doctor
    for each row
begin
	call generate_timetable(NEW.doctor_id);
end //
delimiter ;

-- trigger to refresh timetable in case of update to doctor
delimiter //
drop trigger if exists refresh_timetable //
create trigger refresh_timetable 
	after update on doctor
    for each row
begin
    if ((NEW.availability_id <> OLD.availability_id) and (NEW.availability_id <> '')) then
		call generate_timetable(OLD.doctor_id);
    end if;
end //

drop trigger if exists set_person_id //
create trigger set_person_id 
	before insert on person
    for each row
begin
	declare max_pid int(11) default 0;
    
	case NEW.person_type
		when 'PTNT' 
		then 
			set max_pid = (select max(person_id) from person where person_type = 'PTNT');
            set NEW.person_id = max_pid + 1;
		when 'STFF'
        then
			set max_pid = (select max(person_id) from person where person_type = 'STFF');
            set NEW.person_id = max_pid + 1;
		when 'DOCT'
        then
			set max_pid = (select max(person_id) from person where person_type = 'DOCT');
            set NEW.person_id = max_pid + 1;
		when 'LABA'
        then
			set max_pid = (select max(person_id) from person where person_type = 'LABA');
            set NEW.person_id = max_pid + 1;
		when 'PHRM'
        then
			set max_pid = (select max(person_id) from person where person_type = 'PHRM');
            set NEW.person_id = max_pid + 1;
		else
			SIGNAL sqlstate '45001' set message_text = "No way ! You cannot do this !";
	end case;
end //

delimiter ;


-- EVENTS
-- event to refresh timetable every week sunday night
create event evnet_refresh_timetable
	on schedule 
		every 1 week
        starts '2015-11-29 00:00:00' 
    do call proc_generate_timetable_for_all();





