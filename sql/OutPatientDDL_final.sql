SET FOREIGN_KEY_CHECKS=0 ;
DROP DATABASE IF EXISTS outpatientdbnew;
CREATE DATABASE outpatientdbnew;

USE outpatientdbnew;

DROP TABLE IF EXISTS lab_assistant	CASCADE ;

DROP TABLE IF EXISTS department	CASCADE ;
DROP TABLE IF EXISTS timetable CASCADE ;
DROP TABLE IF EXISTS doctor	CASCADE ;


DROP TABLE IF EXISTS person_address	CASCADE ;
DROP TABLE IF EXISTS patient CASCADE ;
DROP TABLE IF EXISTS staff CASCADE ;
DROP TABLE IF EXISTS person	CASCADE ;
DROP TABLE IF EXISTS person_type CASCADE ;


DROP TABLE IF EXISTS insurance CASCADE ;
DROP TABLE IF EXISTS ailment CASCADE ;
DROP TABLE IF EXISTS availability CASCADE ;
DROP TABLE IF EXISTS availability_group	CASCADE ;

DROP TABLE IF EXISTS appointment CASCADE ;

DROP TABLE IF EXISTS patient_history CASCADE ;
DROP TABLE IF EXISTS laboratory_test CASCADE ;
DROP TABLE IF EXISTS laboratory_report CASCADE ;
DROP TABLE IF EXISTS lab_assignment CASCADE ;




SET FOREIGN_KEY_CHECKS=1 ;

CREATE TABLE `person_type` (
  `person_code` varchar(10) NOT NULL,
  `code_description` varchar(45) DEFAULT NULL,
  `code_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`person_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ailment` (
  `ailment_id` int(11) NOT NULL AUTO_INCREMENT,
  `ailment_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ailment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `availability_group` (
  `availability_group` varchar(10) NOT NULL,
  `monday` tinyint(1) DEFAULT NULL,
  `tuesday` tinyint(1) DEFAULT NULL,
  `wednesday` tinyint(1) DEFAULT NULL,
  `thursday` tinyint(1) DEFAULT NULL,
  `friday` tinyint(1) DEFAULT NULL,
  `saturday` tinyint(1) DEFAULT NULL,
  `sunday` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`availability_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `laboratory_test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_name` varchar(45) DEFAULT NULL,
  `test_charge` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `insurance` (
  `insurance_id` int(11) NOT NULL AUTO_INCREMENT,
  `insurance_name` varchar(45) DEFAULT NULL,
  `insurance_plan` varchar(45) DEFAULT NULL,
  `insurance_copay` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`insurance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `person_first_name` varchar(45) DEFAULT NULL,
  `person_last_name` varchar(45) DEFAULT NULL,
  `person_DOB` date DEFAULT NULL,
  `person_type` varchar(45) DEFAULT NULL,
  `person_addr_id` varchar(45) DEFAULT NULL,
  `person_phone` varchar(15) DEFAULT NULL,
  `person_email` varchar(45) DEFAULT NULL,
  `person_gender` varchar(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `person_email_UNIQUE` (`person_email`),
  KEY `Person_FK1` (`person_type`),
  CONSTRAINT `Person_FK1` FOREIGN KEY (`person_type`) REFERENCES `person_type` (`person_code`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(45) DEFAULT NULL,
  `department_location` varchar(45) DEFAULT NULL,
  `department_head` int(11) DEFAULT NULL,
  PRIMARY KEY (`department_id`),
  KEY `Department_FK1` (`department_id`),
  KEY `Department_FK2` (`department_head`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `availability` (
  `availability_id` int(11) NOT NULL AUTO_INCREMENT,
  `availability_group` varchar(10) DEFAULT NULL,
  `availability_start_time` time DEFAULT NULL,
  `availability_end_time` time DEFAULT NULL,
  PRIMARY KEY (`availability_id`),
  UNIQUE KEY `availability_unique` (`availability_group`,`availability_start_time`,`availability_end_time`),
  KEY `Availability_FK1` (`availability_group`),
  CONSTRAINT `Availability_FK1` FOREIGN KEY (`availability_group`) REFERENCES `availability_group` (`availability_group`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `lab_assistant` (
  `lab_assistant_id` int(11) NOT NULL,
  `DOJ` date DEFAULT NULL,
  `assistant_qualification` varchar(45) DEFAULT NULL,
  `incharge_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`lab_assistant_id`),
  KEY `labassistant_fk1` (`lab_assistant_id`),
  KEY `labassistant_fk2` (`incharge_id`),
  CONSTRAINT `labassistant_fk1` FOREIGN KEY (`lab_assistant_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `labassistant_fk2` FOREIGN KEY (`incharge_id`) REFERENCES `lab_assistant` (`lab_assistant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3000 DEFAULT CHARSET=utf8;

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `DOJ` date DEFAULT NULL,
  PRIMARY KEY (`staff_id`),
  KEY `Staff_ID_FK` (`staff_id`),
  CONSTRAINT `Staff_ID_FK` FOREIGN KEY (`staff_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2000 DEFAULT CHARSET=utf8;

CREATE TABLE `doctor` (
  `doctor_id` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `doctor_DOJ` date DEFAULT NULL,
  `qualification` varchar(45) DEFAULT NULL,
  `availability_id` int(11) DEFAULT NULL,
  `association` varchar(45) DEFAULT NULL,
  `designation` varchar(45) DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`doctor_id`),
  KEY `Doctor_FK1` (`department_id`),
  KEY `Doctor_FK2` (`availability_id`),
  KEY `Doctor_ID_FK` (`doctor_id`),
  CONSTRAINT `Doctor_FK1` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Doctor_FK2` FOREIGN KEY (`availability_id`) REFERENCES `availability` (`availability_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Doctor_ID_FK` FOREIGN KEY (`doctor_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4000 DEFAULT CHARSET=utf8;

ALTER TABLE `department`
  ADD CONSTRAINT `Department_FK2` FOREIGN KEY (`department_head`) REFERENCES `doctor` (`doctor_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `patient_DOR` date DEFAULT NULL,
  `insurance_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  KEY `Patient_ID_FK` (`patient_id`),
  KEY `Patient_FK2` (`insurance_id`),
  CONSTRAINT `Patient_FK2` FOREIGN KEY (`insurance_id`) REFERENCES `insurance` (`insurance_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Patient_ID_FK` FOREIGN KEY (`patient_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;


CREATE TABLE `person_address` (
  `person_id` int(11) NOT NULL,
  `address_code` int(11) NOT NULL,
  `address_line1` varchar(45) DEFAULT NULL,
  `address_line2` varchar(45) DEFAULT NULL,
  `address_city` varchar(45) DEFAULT NULL,
  `address_state` varchar(45) DEFAULT NULL,
  `address_zip` int(11) DEFAULT NULL,
  `address_country` varchar(30) DEFAULT NULL,
  `address_correspondence` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`person_id`,`address_code`),
  KEY `PersonAddress_FK1` (`person_id`),
  CONSTRAINT `PersonAddress_FK1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ;




CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `appt_time` time DEFAULT NULL,
  `appt_day` date DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `ailment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`appointment_id`),
  KEY `Appt_FK1` (`staff_id`),
  KEY `Appt_FK2` (`doctor_id`),
  KEY `Appt_FK3` (`patient_id`),
  KEY `appt_fk4` (`ailment_id`),
  CONSTRAINT `Appt_FK1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Appt_FK2` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctor_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Appt_FK3` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`ailment_id`) REFERENCES `ailment` (`ailment_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;



CREATE TABLE `laboratory_report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `test_date` datetime DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `test_result_date` datetime DEFAULT NULL,
  `test_result` varchar(100) DEFAULT NULL,
  `test_result_details` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`report_id`),
  KEY `labreport_fk1` (`test_id`),
  KEY `labreport_fk2` (`doctor_id`),
  KEY `labreport_fk3` (`patient_id`),
  CONSTRAINT `labreport_fk1` FOREIGN KEY (`test_id`) REFERENCES `laboratory_test` (`test_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `labreport_fk2` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctor_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `labreport_fk3` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

CREATE TABLE `patient_history` (
  `patient_id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `ailment_id` int(11) NOT NULL,
  `symptoms` varchar(100) DEFAULT NULL,
  `diagnosis` varchar(100) DEFAULT NULL,
  `prescription` varchar(45) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `next_visit_date` date DEFAULT NULL,
  PRIMARY KEY (`patient_id`,`appointment_id`),
  KEY `PatientHistory_FK1` (`patient_id`),
  KEY `PatientHistory_FK2` (`appointment_id`),
  KEY `PatientHistory_FK3` (`doctor_id`),
  KEY `PatientHistory_FK4` (`ailment_id`),
  KEY `PatientHistory_FK5` (`test_id`),
  KEY `PatientHistory_FK6` (`report_id`),
  CONSTRAINT `PatientHistory_FK1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `PatientHistory_FK2` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`appointment_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `PatientHistory_FK3` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctor_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `PatientHistory_FK4` FOREIGN KEY (`ailment_id`) REFERENCES `ailment` (`ailment_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `PatientHistory_FK5` FOREIGN KEY (`test_id`) REFERENCES `laboratory_test` (`test_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `PatientHistory_FK6` FOREIGN KEY (`report_id`) REFERENCES `laboratory_report` (`report_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `lab_assignment` (
  `lab_assistant_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`lab_assistant_id`,`report_id`),
  KEY `labassign_fk1` (`report_id`),
  KEY `labassign_fk2` (`lab_assistant_id`),
  CONSTRAINT `labassign_fk1` FOREIGN KEY (`report_id`) REFERENCES `laboratory_report` (`report_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `labassign_fk2` FOREIGN KEY (`lab_assistant_id`) REFERENCES `lab_assistant` (`lab_assistant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `timetable` (
  `doctor_id` int(11) DEFAULT NULL,
  `timeslot` time DEFAULT NULL,
  `monday` char(1) DEFAULT NULL,
  `tuesday` char(1) DEFAULT NULL,
  `wednesday` char(1) DEFAULT NULL,
  `thursday` char(1) DEFAULT NULL,
  `friday` char(1) DEFAULT NULL,
  `saturday` char(1) DEFAULT NULL,
  `sunday` char(1) DEFAULT NULL,
  KEY `idx_timeslot` (`doctor_id`,`timeslot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
