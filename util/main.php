<?php
// Get the document root
$doc_root = $_SERVER['DOCUMENT_ROOT'];

// Get the application path
$uri = $_SERVER['REQUEST_URI'];

$dirs = explode('/', $uri);

//$app_path = '/' . $dirs[1] . '/' . $dirs[2] . '/';
$app_path = '/' . $dirs[1] . '/';

// Set the include path
//set_include_path($doc_root . $app_path);

// Start session to store user and cart data
// session_start();

function addDayswithdate($date,$days){

	$date = strtotime("+".$days." days", strtotime($date));
	return  date("Y-m-d", $date);

}

function resetError(){
	$GLOBALS['errorMessage'] = '';
}
function setError($value){
	$GLOBALS['errorMessage'] = $value;
}
function resetSuccess(){
	$GLOBALS['successMessage'] = '';
}
function setSuccess($value){
	$GLOBALS['successMessage'] = $value;
}
?>