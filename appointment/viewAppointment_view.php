<?php include '../views/header.php';
?>


<div class="container">
          
  <div class="jumbotron" align="center">
    <h3>View Appointmentss</h3>
   </div>

<?php include '../views/usermessages.php';?>


  <form class="form-horizontal" role="form" action="?action=rescheduleAppoinment" method="post">
  <input type="hidden" name="action" value="rescheduleAppoinment" />
  <input type="hidden" name="selectedAilment" id="selectedAilment" value="<?php echo trim($appointments['ailment_id']);?>"/>
  <input type="hidden" name="selectedDoctor" id="selectedDoctor" value="<?php echo trim($appointments['doctor_id']);?>"/>
  <input type="hidden" name="selectedPatient" id="selectedPatient" value="<?php echo trim($appointments['patient_id']);?>"/>
  <fieldset>
  <div class="form-group">
        <label class="control-label col-sm-2" for="appointmentID">Appointment ID:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="appointmentID" id="appointmentID" value="<?php echo trim($appointments['appointment_id']);?>"/>
    </div>
   </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedPatientValue">Patient Name:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control" name="selectedPatientValue" id="selectedPatientValue" value="<?php echo trim($appointments['patient_name']);?>">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="selectedDoctorValue">Doctor Name:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="selectedDoctorValue" id="selectedDoctorValue" value="<?php  echo $appointments['doctor_name']?>"/>
    </div>
   </div>
   
   <div class="form-group">
    <label class="control-label col-sm-2" for="selectedAilmentValue">Ailment Description:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="selectedAilmentValue" id="selectedAilmentValue" value="<?php  echo $appointments['ailment_description']?>"/>
    </div>
   </div>
     
   <div class="form-group">
    <label class="control-label col-sm-2" for="apptDay">Appointment Day:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="apptDay" id="apptDay" value="<?php  echo $appointments['appt_day']?>"/>
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="apptTime">Appointment Time:</label>
    <div class="col-sm-10"> 
      <input type="text" readonly class="form-control" name="apptTime" id="apptTime" value="<?php  echo $appointments['appt_time']?>"/>
    </div>
   </div>
   
   </fieldset>
    
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" class="btn btn-primary" value="Reschedule"/>
    </div>
  </div>
</form>

		
  
</div>
<?php include '../views/footer.php'; ?>