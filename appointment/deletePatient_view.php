<?php include '../views/header.php';?>

<div class="container">
	<div class="jumbotron" align="center">
    <h3>Delete Patient</h3>
   </div>

<?php include '../views/usermessages.php';?>
   
   <form class="form-horizontal" role="form" action="?action=search" method="get">
  	
  	<!-- <div class="form-group" align="center">
    <label class="control-label col-sm-2" for="password">Password:</label>
    <div class="col-sm-2"> 
      <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" required>
    </div>
  	</div> -->
    
	</form>
	
    <?php if (isset($patients)) : ?>
	<table class="table table-bordered table-striped" >
		<thead>
			<tr class='default';>
				<th>Patient ID</th>
				<th>Patient Name</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($patients) == 0) : ?>
				<p>No patients to delete for today. Enjoy!! </p>
			<?php else: ?>
				<?php foreach ($patients as $row) : ?>
				<tr>
					<td><?php  echo $row['person_id']?></td>
					<td><?php  echo $row['personname']?></td>
					<td>
						<form action="?deletePatient" method="get">
						<input type="hidden" name="action" value="deletePatient" />
							<input type="hidden" name="id" value="<?php echo $row['person_id']?>">
							<input type=submit value="Delete" class = "btn btn-danger">
						</form>
					</td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
	<?php endif; ?>
  
</div>
<?php include '../views/footer.php'; ?>