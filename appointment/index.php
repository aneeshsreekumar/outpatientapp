<?php
require_once('../util/main.php');
require_once('../util/tags.php');
require_once('../model/database.php');
require_once('../model/appointment_db.php');
require_once('../model/users_db.php');

/* var_dump($_POST);
var_dump($_GET);


if(isset($_SESSION)){
		var_dump($_SESSION);
} */
session_start();
$doctors = getPerson('DOCT');
$patients = getPerson('PTNT');
$ailments = getAilments();

if (isset($_GET['action']) and $_GET['action'] == 'view')
{
	//$timetable = getTimeTable();
	if(isset($_GET['id']) and $_GET['id'] != ''){
		$appointments = getAppointmentById($_GET['id']);
		//var_dump($appointments);
		include('viewAppointment_view.php');
	}else{
		$appointments = getAppointments();
		include('appointment_view.php');
	}	
	
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'search')
{
	//$timetable = getTimeTable();
	$appointments = getAppointmentsForPatient($_GET['patientFirstName']);
	include('appointment_view.php');
	exit();
}


if (isset($_GET['action']) and $_GET['action'] == 'cancelAppointment')
{
	//echo 'inside cancelAppointment';
	cancelAppointment($_GET['id']);
	//$timetable = getTimeTable();
	$appointments = getAppointments();
	include('appointment_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'newAppointment')
{
	
	//$timetable = getTimeTable();
	
	include('newappointment_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'generateBillPatient')
{

	//echo 'came inside generateBill';
	$billdetails = getBillforCustomer($_GET['id']);
	include('generateBill_view.php');
	exit();
}

if (isset($_POST['action']) and $_POST['action'] == 'getDoctorTimeTable')
{

	
	$actionValue = 'createAppointment';
	$selectedDoctorValue = $_POST['selectedDoctorValue'];
	$selectedPatientValue = $_POST['selectedPatientValue'];
	$selectedAilmentValue = $_POST['selectedAilmentValue'];
	
	$_SESSION['selectedAilment'] = $_POST['selectedAilment'];
	$_SESSION['selectedDoctor']  = $_POST['selectedDoctor'];
	$_SESSION['selectedPatient'] = $_POST['selectedPatient'];
	
	 
	
	
	
	$dayofweeknum = date("w",time());
	$currenttime = date("H:i:m",time());
	//echo 'dayofweeknum:'.$dayofweeknum.':'.':currentime:'.$currenttime;
	$timetable = getTimeTable($_POST['selectedDoctor']);
	
	
	
	include('doctortimetable_view.php');
	exit();
}

if (isset($_POST['action']) and $_POST['action'] == 'createAppointment')
{

	//$timetable = getTimeTable();
	$currdate = date('Y-m-d',time());
	$apptDate = addDayswithdate($currdate, $_POST['selectedTimeSlot']);
	//echo 'added date:'.$apptDate.':curreent date:'.$currdate;
	$appointment_id = newAppointment($_SESSION['selectedPatient'], $_SESSION['selectedDoctor'] , $_SESSION['selectedAilment'], $_POST['selectedTimeSlot'], $currdate);
	
	unset($_SESSION['selectedAilment']);
	unset($_SESSION['selectedDoctor']);
	unset($_SESSION['selectedPatient']);	
	setSuccess('Appointment created successfully, New Appointment ID:'.$appointment_id);
	header('Location: ?action=view&id='.$appointment_id);
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'rescheduleAppoinment')
{

	//echo 'inside rescheduleApp';
	$actionValue = 'updateAppointment';
	$selectedDoctorValue = $_POST['selectedDoctorValue'];
	$selectedPatientValue = $_POST['selectedPatientValue'];
	$selectedAilmentValue = $_POST['selectedAilmentValue'];
	
	$_SESSION['selectedAilment'] = $_POST['selectedAilment'];
	$_SESSION['selectedDoctor']  = $_POST['selectedDoctor'];
	$_SESSION['selectedPatient'] = $_POST['selectedPatient'];
	$_SESSION['appointmentID'] = $_POST['appointmentID'];
	
	$dayofweeknum = date("w",time());
	$currenttime = date("H:i:m",time());
	//echo 'dayofweeknum:'.$dayofweeknum.':'.':currentime:'.$currenttime;
	$timetable = getTimeTable($_POST['selectedDoctor']);



	include('doctortimetable_view.php');
	exit();
}

if (isset($_POST['action']) and $_POST['action'] == 'updateAppointment')
{

	//$timetable = getTimeTable();
	$currdate = date('Y-m-d',time());
	$apptDate = addDayswithdate($currdate, $_POST['selectedTimeSlot']);
	//echo 'added date:'.$apptDate.':curreent date:'.$currdate;
	
	$appointment_id = rescheduleAppointment($_SESSION['appointmentID'],$currdate,$_POST['selectedTimeSlot']);

	unset($_SESSION['selectedAilment']);
	unset($_SESSION['selectedDoctor']);
	unset($_SESSION['selectedPatient']);
	unset($_SESSION['appointmentID']);
	
	setSuccess('Appointment created successfully, New Appointment ID:'.$appointment_id);
	header('Location: ?action=view&id='.$appointment_id);
	exit();
}


if (isset($_GET['action']) and $_GET['action'] == 'deleteUserView')
{
	//$timetable = getTimeTable();
	//$appointments = getAppointmentsForPatient($_GET['patientFirstName']);
	include('deletePatient_view.php');
	exit();
}

if (isset($_GET['action']) and $_GET['action'] == 'deletePatient')
{
	if(isset($_GET['id']) and $_GET['id'] != ''){
		$result = deleteUser($_GET['id']);
		if($result){
			setSuccess('Patient Deleted Successfully!');
			$patients = getPerson('PTNT');
		}else{
			setError('Delete User Failed!');
		}
	}else{
		setError('No ID given!');
	}
	include('deletePatient_view.php');
	exit();
}






?>