<?php include '../views/header.php';?>

<div class="container">
	<div class="jumbotron" align="center">
    <h3>Appointments</h3>
   </div>

<?php include '../views/usermessages.php';?>
   
   <form class="form-horizontal" role="form" action="?action=search" method="get">
  	<div class="form-group" align="center">
    <label class="control-label col-sm-2" for="userlogin">Patient First Name:</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" name="patientFirstName" id="patientFirstName" placeholder="Enter Patient First Name" required autofocus="autofocus">
    </div>
  	</div>
  	<!-- <div class="form-group" align="center">
    <label class="control-label col-sm-2" for="password">Password:</label>
    <div class="col-sm-2"> 
      <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" required>
    </div>
  	</div> -->
      	<input type="hidden" name="action" value="search" />
  	<div class="btn-toolbar">
  		<div class="btn-group">
    
      	<input type="submit" class="btn btn-primary" value="Search"/>
      	<a href="?action=newAppointment" class="btn btn-primary">New Appointment</a>
   		 </div>
             
    </div>
  	
	</form>
	<?php if (isset($timetable)) : ?>
	<table class="table table-bordered table-striped" >
		<thead>
			<tr class='default';>
				<th>Doctor Name</th>
				<th>Doctor Id</th>
				<th>Time Slot</th>
				<th>Monday</th>
				<th>Tuesday</th>
				<th>Wednesday</th>
				<th>Thursday</th>
				<th>Friday</th>
				<th>Saturday</th>
				<th>Sunday</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($timetable) == 0) : ?>
				<p>Error Fetching Time Table. Please contact System Administrator</p>
			<?php else: ?>
				<?php foreach ($timetable as $row) : ?>
				<tr class="success">
					<td><?php  echo $row['doctor_name']?></td>
					<td><?php  echo $row['doctor_id']?></td>
					<td><?php  echo $row['timeslot']?></td>
					<td><?php  if ($row['monday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
					<td><?php  if ($row['tuesday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
					<td><?php  if ($row['wednesday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
					<td><?php  if ($row['thursday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
					<td><?php  if ($row['friday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
					<td><?php  if ($row['saturday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
					<td><?php  if ($row['sunday'] == 0): echo 'NA'; else: echo 'YES'; endif;?></td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
    <?php endif; ?>
    <?php if (isset($appointments)) : ?>
	<table class="table table-bordered table-striped" >
		<thead>
			<tr class='default';>
				<th>Patient Name</th>
				<th>Doctor Name</th>
				<th>Appointment ID</th>
				<th>Appointment Time</th>
				<th>Appointment Day</th>
				<th>Status</th>
				<th>Cancel</th>
				<th>Reschedule</th>
				<th>Generate Bill</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($appointments) == 0) : ?>
				<p>No appointments for today. Enjoy!! </p>
			<?php else: ?>
				<?php foreach ($appointments as $row) : ?>
				<tr>
					<td><?php  echo $row['patient_name']?></td>
					<td><?php  echo $row['doctor_name']?></td>
					<td><?php  echo $row['appointment_id']?></td>
					<td><?php  echo $row['appt_time']?></td>
					<td><?php  echo $row['appt_day']?></td>
					<td><?php  echo $row['status']?></td>
					<td>
						<form action="?cancelAppointment" method="get">
							<input type="hidden" name="action" value="cancelAppointment" />
							<input type="hidden" name="id" value="<?php echo $row['appointment_id']?>">
							<input type=submit value=Cancel class="btn btn-warning">
						</form>
					</td>
					<td>
						<form action="?rescheduleAppointment" method="get">
						<input type="hidden" name="action" value="rescheduleAppointment" />
							<input type="hidden" name="id" value="<?php echo $row['appointment_id']?>">
							<input type=submit value=Reschedule class = "btn btn-primary">
						</form>
					</td>
					<td>
						<form action="?generateBillPatient" method="get">
						<input type="hidden" name="action" value="generateBillPatient" />
							<input type="hidden" name="id" value="<?php echo $row['appointment_id']?>">
							<input type=submit value="Get Bill" class = "btn btn-primary">
						</form>
					</td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
    </table>
	<?php endif; ?>
  
</div>
<?php include '../views/footer.php'; ?>