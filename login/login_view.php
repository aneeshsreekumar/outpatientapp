<?php include 'views/header.php';?>

<div class="container">

<?php if (array_key_exists ( 'errorMessage', $GLOBALS ) and ! empty ( $GLOBALS ['errorMessage'] )) : ?>
	<div class="alert alert-danger">
		<strong>
			
		<?php echo $GLOBALS ['errorMessage'];?>
		
		</strong>
	</div>
<?php endif;?>
	<div class="jumbotron" align="center">
    <h1>Welcome to Out Patient App</h1>
   </div>
  <form class="form-horizontal" role="form" action="index.php" method="post">
  <div class="form-group" align="center">
    <label class="control-label col-sm-2" for="userlogin">User Login:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control" name="userlogin" id="userlogin" placeholder="Enter User Login" required autofocus="autofocus">
    </div>
  </div>
  <div class="form-group" align="center">
    <label class="control-label col-sm-2" for="password">Password:</label>
    <div class="col-sm-2"> 
      <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" required pattern="[a-zA-Z0-9]{5,12}" title="5-12 alphanumeric characters only allowed">
    </div>
  </div>
   
  <div class="form-group" align="center"> 
    <div class="col-sm-1">
    	<input type="hidden" name="action" value="login" />
      <input type="submit" class="btn btn-primary" value="Login"/>
    </div>
    <div class="col-sm-1">
      <a href="register/?action=register" class="btn btn-default" >Register</a>
    </div>
  </div>
</form>
  
</div>
<?php include 'views/footer.php'; ?>